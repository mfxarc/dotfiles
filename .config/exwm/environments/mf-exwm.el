;;; mf-exwm.el --- My mf-exwm.el setup
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: março 18, 2022
;;
;;; Commentary:
;;
;;  My configuration for mf-exwm.el
;;
;;; Code:

(straight-use-package '(app-launcher :type git :host github :repo "SebastienWae/app-launcher"))
(straight-use-package '(exmw-outer-gaps :type git :host github :repo "lucasgruss/exwm-outer-gaps"))

(setq exwm-outer-gaps-width '[0 0 4 0])
(add-hook 'exwm-init-hook 'exwm-outer-gaps-mode)

(setq app-launcher--annotation-function 'app-launcher--annotation)

(defun app-launcher--annotation (choice)
  "Default function to annotate the completion CHOICEs."
  (let ((str (cdr (assq 'comment (gethash choice app-launcher--cache)))))
    (when str (concat "\t - " (propertize str 'face '(:inherit completions-annotations))))))

(setq exwm-manage-force-tiling t)
(setq mouse-autoselect-window nil)
(setq focus-follows-mouse t)
(setq exwm-workspace-warp-cursor t)

;; If a popup does happen, don't resize windows to be equal sized
(setq display-buffer-base-action '(display-buffer-reuse-mode-window
				   display-buffer-reuse-window
				   display-buffer-same-window))

(setq exwm-layout-show-all-buffers t)
(setq exwm-workspace-show-all-buffers t)
(setq exwm-workspace-number 6) ;; Set number of workspaces

(setq exwm-input-prefix-keys nil)

(add-hook 'exwm-manage-finish-hook 'exwm-input-grab-keyboard)

;; Fix C-c in windows
(define-key exwm-mode-map (kbd "C-c") nil)
;;
(exwm-def '("s-f"   find-file)
	  '("s-e"   consult-buffer)
	  '("s-w"   execute-extended-command)
	  '("s-p"   spawn)
	  '("s-j"   evil-window-next)
	  '("s-k"   evil-window-prev)
	  '("s-h"   shrink-window)
	  '("s-l"   expand-window)
	  '("s-s"   exwm-split-window-below)
	  '("s-v"   exwm-split-window-right)
	  '("s-ç"   exwm-toggle-window-split)
	  '("C-s-ç" exwm-toggle-single-window)
	  '("C-s-j" windmove-swap-states-down)
	  '("C-s-k" windmove-swap-states-up)
	  '("C-s-l" windmove-swap-states-right)
	  '("C-s-h" windmove-swap-states-left)
	  '("s-u"   exwm-view-previous-buffer)
	  '("s-c"   kill-window)
	  '("s-o"   exwm-view-next-ws)
	  '("s-i"   exwm-view-prev-ws)
	  '("C-s-i" shift-to-prev-ws)
	  '("C-s-o" shift-to-next-ws)
	  '("s-r" dired-jump)
	  '("C-SPC" app-launcher-run-app)
	  '("s-SPC" exwm-floating-toggle-floating))

(exwm-cmd '("s-<return>"  "qutebrowser")
	  '("<pause>" "pactl -- set-sink-volume 0 -2%")
	  '("<Scroll_Lock>" "pactl -- set-sink-volume 0 +2%")
	  '("<print>" "dmmaim")
	  '("S-<print>" "sleep 2 && notify-send 'Screenshot taken' && shotgun")
	  '("<f4>" "colorclip")
	  '("<f3>" "picket"))

(defun exwm-window-rules ()
  (pcase exwm-class-name
    ((string= (or "Emacs" "nvim" "notion-app-enhanced" "Zathura") (exwm-shift-to 1)))
    ((string= (or "Min" "firefox-nightly" "LibreWolf" "qutebrowser" "Ferdi") (exwm-shift-to 2)))
    ((string= (or "St" "Spacefm" "Timeshift-gtk") (exwm-shift-to  3)))
    ((string= (or "wpsoffice" "libreoffice" "MuseScore3" "Anki" "wps") (exwm-shift-to 4)))
    ((string= (or "mpv" "Ario") (exwm-shift-to 5)))
    ((string= (or "Gimp" "Gimp-2.10" "Gimp-2.99" "Akira" "Krita" "Darktable") (exwm-shift-to 6)))
    (_ (message "No rules for current window"))))

(add-hook 'exwm-manage-finish-hook 'exwm-window-rules)

(exwm-enable)

;; Let emacs handle gpg
(straight-use-package 'pinentry)
(require 'epa-file)
(setenv "GPG_AGENT_INFO" nil)  ;; use emacs pinentry
(setq auth-source-debug t)
(setq epg-gpg-program "gpg2")  ;; not necessary
(setq epa-pinentry-mode 'loopback)
(setq epg-pinentry-mode 'loopback)
(with-eval-after-load 'org
  (require 'org-crypt)
  (org-crypt-use-before-save-magic))
(epa-file-enable)
(pinentry-start)

;; Programs to start at init
;; (progn (spawn "hsetroot -cover ~/pix/gruvbox/europeana.jpg")
;;        (spawn "xsetroot -cursor_name left_ptr")
;;        (spawn "picom -b")
;;        (spawn "dunst")
;;        (spawn "xset r rate 170 40")
;;        (spawn "/usr/lib/kdeconnectd")
;;        (spawn "fcitx5")
;;        (spawn "xrdb ~/.config/xresources/gruvbox")
;;        (spawn "dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY")
;;        (spawn "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 >/dev/null 2>&1")
;;        (spawn "gnome-keyring-daemon -s")
;;        (spawn "sleep 5 && setxkbmap -layout br -option 'ctrl:swapcaps,altwin:swap_alt_win,altwin:alt_win'")
;; (spawn "killall -9 xmobar ; xmobar ~/.config/xmobar/exwmrc"))

;; https://github.com/Koekelas/dotfiles/blob/master/emacs.org
(provide 'mf-exwm)
;;; mf-exwm.el ends here)
