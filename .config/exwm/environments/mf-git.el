;;; mf-git.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 18, 2022
;;
;;; Commentary:
;;  
;;  My configuration for mf-git.el
;;  
;;; Code:

(setup (:straight magit)
  (:with-hook magit-process-mode-hook
    (:hook 'goto-address-mode))
  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (setq transient-default-level 5)
  (setq magit-diff-refine-hunk nil)
  (setq magit-save-repository-buffers nil)
  (setq magit-revision-insert-related-refs nil)
  (setq transient-display-buffer-action '(display-buffer-below-selected))
  (setq magit-bury-buffer-function #'magit-mode-quit-window)
  (:with-map magit-status-mode-map
    (:bind "j" 'magit-next-line
	   "k" 'magit-previous-line
	   "l"     'magit-section-toggle
	   "h"     'magit-diff-visit-file
	   "C-r"   'magit-refresh-all
	   "C-l"   'magit-log-all
	   "SPC"   'nil))
  (:with-map magit-log-mode-map
    (:bind "l"   'magit-show-commit)))

(require 'magit)
(setq bare-git-dir (concat "--git-dir=" (expand-file-name "~/.config/git/dotfiles")))
(setq bare-work-tree (concat "--work-tree=" (expand-file-name "~/")))

(defun magit-status-here ()
  "Like `magit-status' but with non-nil magit-status-goto-file-position"
  (interactive)
  (require 'magit-status)
  (setq magit-git-global-arguments (remove bare-git-dir magit-git-global-arguments))
  (setq magit-git-global-arguments (remove bare-work-tree magit-git-global-arguments))
  (call-interactively #'magit-status))

(defun magit-status-bare ()
  "Like `magit-status' but with for a bare repository."
  (interactive)
  (require 'magit-status)
  (add-to-list 'magit-git-global-arguments bare-git-dir)
  (add-to-list 'magit-git-global-arguments bare-work-tree)
  (call-interactively #'magit-status))

(setup (:straight dired-git-info)
  (setq dgi-auto-hide-details-p nil)
  (add-hook 'dired-after-readin-hook 'dired-git-info-auto-enable))
(setup (:straight dired-git))

(provide 'mf-git)
;;; mf-git.el ends here
