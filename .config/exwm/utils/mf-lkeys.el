;;; mf-lkeys.el --- My mf-lkeys.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;;
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: janeiro 12, 2022
;; Modified: janeiro 12, 2022
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:
(setup (:straight general)
  (require 'general))
(defun none () (interactive))
(general-define-key
 :states '(normal visual emacs motion insert)
 :keymaps '(helpful-mode-map dired-mode-map image-mode-map special-mode-map messages-buffer-mode-map elfeed-show-mode-map)
 "q" 'kill-buffer-and-window)
(general-define-key
 :states '(normal 'visual)
 :keymaps 'emacs-lisp-mode-map
 "TAB"   '(hs-toggle-hiding :which-key "Fold"))
(general-define-key
 :states 'normal
 "n f"  '(format-buffer :which-key "Format buffer"))
(general-define-key
 :states '(normal visual motion emacs)
 :prefix "n"
 :keymaps 'emacs-lisp-mode-map
 ""   '(:ignore t :which-key "Elisp")
 "e"  '(macrostep-expand :which-key "Expand macro")
 "b"  '(eval-buffer :which-key "Eval buffer")
 "d"  '(eval-defun :which-key "Eval defun")
 "r"  '(eval-region :which-key "Eval region")
 "l"  '(eval-last-sexp :which-key "Eval last expression"))
;;; --------------------------------------------------
(general-define-key
 :states 'normal
 :keymaps 'org-mode-map
 "TAB" 'org-cycle
 "C-<return>" 'org-meta-return
 "C-l" 'org-metaright
 "C-h" 'org-metaleft)
(general-define-key
 :states 'normal
 :keymaps 'org-mode-map
 :predicate '(org-in-src-block-p)
 "n f" '(org-cleanup :which-key "Format source block"))
(general-define-key :keymaps 'evil-motion-state-map "RET" 'nil)
(general-define-key :states '(normal visual) :keymaps 'org-mode-map "n" '(org-menu :which-key "Org"))
 (general-define-key
  :states 'normal
  :prefix "C-n"
  :keymaps 'org-mode-map
  :predicate '(org-table-p)
  "l" '(org-table-insert-hline :which-key "Insert line")
  "c" '(org-table-insert-column :which-key "Insert column")
  "r" '(org-table-insert-row :which-key "Insert row")
  "d" '(org-table-delete-column :which-key "Delete column")
  "f" '(org-table-eval-formula :which-key "Insert formula")
  "i" '(org-table-field-info :which-key "Field info")
  "s" '(org-table-sum :which-key "Sum column"))
(general-define-key
 :states 'normal
 :keymaps 'org-src-mode-map
 "C-c C-c" 'org-edit-src-exit)
(general-define-key
 :keymaps 'org-agenda-mode-map
 "SPC" 'nil
 "g"   'nil
 "j"   'org-agenda-next-line
 "k"   'org-agenda-previous-line
 "C-j" 'org-agenda-next-item
 "C-k" 'org-agenda-previous-item
 "l"   '(org-agenda-show :which-key "Show entry")
 "g"   '(:ignore t :which-key "Go to")
 "g d" '(org-agenda-goto-date :which-key "Date")
 "g h" '(org-agenda-goto-today :which-key "Today"))


;;; --------------------------------------------------
(general-define-key
 :states '(normal visual motion emacs)
 :prefix "n"
 :keymaps 'LaTeX-mode-map
 "n" '(TeX-insert-macro :which-key "Insert macro"))
(general-define-key
 :states '(normal 'visual)
 :keymaps 'LaTeX-mode-map
 "TAB"   '(TeX-fold-dwim :which-key "Fold macro"))

;;; --------------------------------------------------
(general-define-key
 :states 'normal
 :keymaps 'dired-mode-map
 ;;-- NAVIGATION
 "u"   'evil-scroll-up
 "d"   'evil-scroll-down
 "h"   'dired-up-directory
 "l"   'dired-find-file
 "C-l" 'dired-find-file-other-window
 ;;-- MARKING
 "f"   'dired-mark
 "n"   'dired-unmark
 "t"   'dired-toggle-marks
 ;;-- INTERFACE
 "s"   'dired-hide-dotfiles-mode
 "C-h" 'dired-hide-details-mode
 "C-f" 'dired-sort
 ;;-- CREATE
 "c d" 'dired-create-directory
 "c f" 'dired-create-empty-file
 ;;-- COPY/MOVE
 "o"   'dired-do-hardlink
 "y"   'dired-ranger-copy
 "Y"   'dired-do-copy
 "C-y" 'dired-copy-path
 "x"   'dired-ranger-move
 ;;-- PASTE
 "p"   'dired-ranger-paste
 ;;-- DELETE
 "C-d" 'dired-do-delete
 ;;-- EXTERNAL
 "C-t" 'dired-term
 "a"   'wdired-change-to-wdired-mode
 "i"   'wdired-change-to-wdired-mode
 "M"   'dired-chmod
 "O"   'dired-chown
 "ed"  'epa-dired-do-decrypt
 "ee"  'epa-dired-do-encrypt
 "z"   'dired-do-compress)

(general-define-key
 :keymaps 'vertico-map
 "C-j" 'vertico-next
 "C-k" 'vertico-previous
 "C-f" 'vertico-exit)
(general-define-key
 :keymaps 'minibuffer-local-map
 "C-h" 'minibuffer-backward-kill)

;;; ---------------
(general-define-key
 :states '(normal motion)
 :keymaps 'elfeed-search-mode-map
 "l" 'elfeed-search-show-entry
 "h" 'none
 "r" 'elfeed-search-untag-all-unread
 "u" 'elfeed-search-tag-all-unread
 "s" 'elfeed-search-live-filter
 "y" 'elfeed-copy-link
 "C-y" 'elfeed-copy-org-link
 "q" 'kill-buffer-and-window
 "o" 'elfeed-open
 "p" 'elfeed-open-browser
 "C-r" 'elfeed-update)

;;; ---------------
(general-define-key
 :states 'normal
 :keymaps 'eww-mode-map
 "C-f" 'shrface-headline-consult)

;;; ---------------
(general-define-key
 :states 'normal
 :keymaps 'emms-playlist-mode-map
 "l" 'emms-playlist-mode-play-smart
 "j" 'evil-next-line
 "k" 'evil-previous-line
 "C-k" 'emms-previous
 "C-j" 'emms-next)

(general-define-key
 :keymaps 'magit-status-mode-map
 "p"   'magit-push
 "C-d" 'evil-scroll-down
 "C-u" 'evil-scroll-up
 "v"   'evil-visual-line)

(general-define-key
 :states '(normal visual motion emacs)
 :keymaps 'ibuffer-mode-map
 "h"  'kill-current-buffer
 "f"  'ibuffer-mark-forward
 "d"  'ibuffer-do-delete
 "l"  'ibuffer-do-view
 "j"  'evil-next-line
 "k"  'evil-previous-line)

(general-define-key
 :states '(normal visual emacs motion insert)
 :keymaps 'magit-log-mode-map
 "j"   'evil-next-line
 "k"   'evil-previous-line)

(general-define-key :keymap 'corfu-map "C-l" 'corfu-doc-toggle)

(general-define-key
 :keymap 'vertico-map
 "M-j" 'vertico-next-group
 "M-k" 'vertico-previous-group)

(general-define-key
 :states 'insert
 :keymap '(vterm-mode-map override)
 "C-j" 'vterm-send-C-j)

(general-define-key
 :keymaps 'xwidget-webkit-mode-map
 "j" 'xwidget-webkit-scroll-up-line
 "k" 'xwidget-webkit-scroll-down-line)

(provide 'mf-lkeys)
;;; mf-lkeys.el ends here
