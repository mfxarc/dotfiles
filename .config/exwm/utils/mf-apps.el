;;; mf-apps.el --- My mf-apps.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;;
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: janeiro 12, 2022
;; Modified: janeiro 12, 2022
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:
;;; EWW
(setup (:straight shrface))
(add-hook 'eww-after-render-hook 'shrface-mode)
(setq eww-search-prefix "https://duckduckgo.com/?q=")
(setq shr-indentation 10)
(setq shr-width 100)
(setq shr-use-fonts nil)
(setq shr-inhibit-images t)
(setq shr-use-colors nil)
(setq shrface-toggle-bullets t)
(setq url-privacy-level '(email agent cookies lastloc))
;;; Everywhere
(setup (:straight emacs-everywhere))
(provide 'mf-apps)
;;; mf-apps.el ends here
