;;; mf-sh.el --- My shell and terminal related tools setup -*- lexical-binding: t; -*-

(setup (:straight vterm-toggle))
(setup (:straight vterm)
  (:also-straight eterm-256color)
  (setq vterm-term-environment-variable "eterm-color")
  (setq vterm-kill-buffer-on-exit t)
  (setq vterm-max-scrollback 5000)
  (setq confirm-kill-process 'nil))
(defun make-bottom-window ()
  (progn (exwm-toggle-window-split)
	 (make-window-half)))
(add-hook 'vterm-toggle-show-hook 'make-bottom-window)

(setup (:straight restart-emacs))

(setup (:git async "jwiegley/emacs-async"))

(provide 'mf-sh)
