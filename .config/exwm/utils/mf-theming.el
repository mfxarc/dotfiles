;;; mf-theming.el --- My mf-theming.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 24, 2022
;;
;;; Commentary:
;;  
;;  My configuration for mf-theming.el
;;  
;;; Code:

(set-face-attribute 'org-level-1 nil :height 1.0)
(set-face-attribute 'org-level-2 nil :height 1.0)
(set-face-attribute 'org-level-3 nil :height 1.0)
(set-face-attribute 'org-level-4 nil :height 1.0)
(set-face-attribute 'org-level-5 nil :height 1.0)
(set-face-attribute 'org-level-6 nil :height 1.0)
(set-face-attribute 'org-level-7 nil :height 1.0)
(set-face-attribute 'org-level-8 nil :height 1.0)
(set-face-attribute 'org-block-begin-line nil :underline nil :overline nil)
(set-face-attribute 'org-block-end-line nil :underline nil :overline nil)
(set-face-attribute 'org-meta-line nil :underline nil :overline nil)
(set-face-attribute 'org-checkbox nil :box nil)
(setq-default prettify-symbols-alist
	      '(("#+begin_quote" . "")
		("#+end_quote" . "")
		("#+begin_src" . "")
		("#+end_src" . "")
		("#+begin_myframe" . "")
		("#+end_myframe" . "")
		("#+title:" . "")
		("#+nota:" . "")
		("#+filetags:" . "")
		("#+tags:" . "")
		("#+type:" . "")
		("#+author:" . "")
		("#+date:" . "")
		("#+time:" . "")
		("#+TBLFM:" . "")
		("#+setupfile:" . "")
		(":PROPERTIES:" . "")
		("[ ]" . "")
		("[X]" . "")))
(set-face-attribute 'fixed-pitch nil :font "Iosevka" :height 170)
(set-face-attribute 'variable-pitch nil :font "Roboto" :height 170)
(set-fontset-font t 'hangul '("Sarasa Mono K" . "unicode-bmp"))
(set-fontset-font t 'symbol '("Symbola" . "unicode-bmp"))
(set-face-attribute 'default nil :font "Iosevka" :height 170)
(provide 'mf-theming)
;;; mf-theming.el ends here
