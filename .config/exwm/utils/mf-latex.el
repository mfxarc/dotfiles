;;; mf-latex.el --- My mf-latex.el setup -*- lexical-binding: t; -*-

(setup (:git litex-mode "Atreyagaurav/litex-mode")
  (require 'litex-mode))

(setup (:straight auctex)
  (add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))
  (:with-hook LaTeX-mode-hook
    (:hook 'TeX-fold-mode))
  (:with-hook after-save-hook
    (:hook 'latex-compile))
  (setq TeX-engine 'xetex)
  (setq TeX-command "xelatex")
  (setq TeX-view-program-selection '(output-pdf "zathura"))
  (setq TeX-source-correlate-start-server nil)
  (setq TeX-electric-sub-and-superscript t)
  (setq TeX-save-query nil)
  (:when-loaded
    (add-to-list 'TeX-command-list '("XeLaTeX" "%`xelatex --synctex=1%(mode)%' %t" TeX-run-TeX nil t))
    (add-to-list 'TeX-view-program-list '("zathura" openwith-file-handler))
    (add-to-list 'TeX-view-program-selection '(output-pdf "zathura"))))

;; https://orgmode.org/worg/org-tutorials/org-latex-export.html
(with-eval-after-load 'org
  ;; (add-to-list 'org-latex-logfiles-extensions "tex")
  (add-to-list 'org-latex-listings-langs '(shell . "bash"))
  (setq org-preview-latex-default-process 'dvisvgm)
  (setq org-preview-latex-image-directory ".ltximg/")
  (setq org-latex-preview-ltxpng-directory ".ltximg/")
  (setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
  (setq org-latex-pdf-process '("xelatex -interaction=batchmode %f"))
  (setq org-latex-compiler "xelatex")
  (setq org-latex-remove-logfiles t)
  (setq org-latex-listings t)
  (setq org-export-with-toc nil)
  (setq org-export-coding-system 'utf-8-unix)
  (setq org-latex-classes '(("article"
			     "\\documentclass{article}"
			     ("\\section{%s}" . "\\section*{%s}")
			     ("\\subsection{%s}" . "\\subsection*{%s}")
			     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			     ("\\paragraph{%s}" . "\\paragraph*{%s}")
			     ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
			    ("book"
			     "\\documentclass{memoir}"
			     ("\\section{%s}" . "\\section*{%s}")
			     ("\\subsection{%s}" . "\\subsection*{%s}")
			     ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
			     ("\\paragraph{%s}" . "\\paragraph*{%s}")
			     ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))))
(provide 'mf-latex)
;;; mf-latex.el ends here
