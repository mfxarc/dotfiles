;;; mf-exwm-utils.el --- My mf-exwm-utils setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: março 18, 2022
;;
;;; Commentary:
;;
;;  My configuration for mf-exwm-utils.el
;;
;;; Code:

(defun spawn (COMMAND)
  "Run shell command async."
  (interactive "s$ ")
  (call-process-shell-command (concat COMMAND " &") nil 0 nil))

(defun exwm-view-prev-ws ()
  "Go to previous workspace."
  (interactive)
  (when (> exwm-workspace-current-index 0)
    (exwm-workspace-switch (- exwm-workspace-current-index 1))))

(defun exwm-view-next-ws ()
  "Go to next workspace."
  (interactive)
  (when (< exwm-workspace-current-index (- exwm-workspace-number 1))
    (exwm-workspace-switch (+ exwm-workspace-current-index 1))))

(defun shift-to-next-ws ()
  "Move buffer to next workspace."
  (interactive)
  (when (< exwm-workspace-current-index (- exwm-workspace-number 1))
    (exwm-workspace-move-window (+ exwm-workspace-current-index 1))))

(defun shift-to-prev-ws ()
  "Move buffer to prev workspace."
  (interactive)
  (when (> exwm-workspace-current-index 0)
    (exwm-workspace-move-window (- exwm-workspace-current-index 1))))

(defun exwm-del-next-workspace ()
  "Delete next workspace."
  (interactive)
  (when (< exwm-workspace-current-index (- exwm-workspace-number 1))
    (exwm-workspace-delete (+ exwm-workspace-current-index 1))))

(defun exwm-del-prev-workspace ()
  "Delete previous workspace."
  (interactive)
  (when (> exwm-workspace-current-index 0)
    (exwm-workspace-delete (- exwm-workspace-current-index 1))))

(defun exwm-split-window-right ()
  "Create window at right."
  (interactive)
  (progn (split-window-right)
	 (windmove-right)
	 (switch-to-buffer "scratch")))

(defun exwm-split-window-below ()
  "Create window at bottom."
  (interactive)
  (progn (split-window-below)
	 (windmove-down)
	 (switch-to-buffer "scratch")))

(defun exwm-view-previous-buffer ()
  "Switch to previous buffer."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

;; ---------- CLOSE OTHER WINDOWS AND KEEP ONLY THE FOCUSED
(defun exwm-toggle-single-window ()
  (interactive)
  (if (= (count-windows) 1)
      (when single-window--last-configuration
	(set-window-configuration single-window--last-configuration))
    (setq single-window--last-configuration (current-window-configuration))
    (delete-other-windows)))

;; ---------- TOGGLE BETWEEN HORIZONTAL/VERTICAL SPLIT
(defun exwm-toggle-window-split ()
  "Switch between vertical and horizontal split only works for 2 windows"
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))

(defun make-window-half ()
  (interactive)
  (other-window 1)
  (enlarge-window (/ (window-height (next-window)) 2))
  (other-window 1))

(defun make-horizontal-half ()
  (interactive)
  (other-window 1)
  (enlarge-window-horizontally (/ (window-height (next-window)) 2))
  (other-window 1))

(defun kill-window ()
  (interactive)
  (if (>= (count-windows) 2)
      (progn (kill-buffer (current-buffer))
	     (delete-window))
    (kill-buffer (current-buffer))))

(defun bind-key (&rest args)
  "Bind KEY to COMMAND globally."
  (dolist (arglist args)
    (let* ((key (nth 0 arglist))
	   (command (nth 1 arglist)))
      (global-set-key (kbd key) command))))

(defun expand-window ()
  (interactive)
  (when (>= (count-windows) 2)
    (enlarge-window 2 t)))

(defun shrink-window ()
  (interactive)
  (when (>= (count-windows) 2)
    (enlarge-window -2 t)))

(defun exwm-window-rules ()
  (interactive)
  (exwm-layout-hide-mode-line)
  (pcase exwm-class-name
    ;; HOME WORKSPACE
    ;; DEV WORKSPACE
    ("St" (exwm-workspace-move-window 0)(exwm-workspace-switch 0))))

(defun exwm-shift-to (WS_INDEX)
  "Shift window to workspace N."
  (let ((wsnum (- WS_INDEX 1)))
    (progn (exwm-workspace-move-window wsnum)
	   (exwm-workspace-switch wsnum))))

(defun exwm-def (&rest args)
  "Bind KEY to FUNCTION globally."
  (dolist (arglist args)
    (let* ((key (nth 0 arglist))
	   (command (nth 1 arglist)))
      (exwm-input-set-key (kbd key) command)
      (global-set-key (kbd key) command)
      (define-key exwm-mode-map (kbd key) command))))

(defun exwm-cmd (&rest args)
  "Bind KEY to COMMAND globally."
  (dolist (arglist args)
    (let* ((key (nth 0 arglist))
	   (command (nth 1 arglist)))
      (define-key exwm-mode-map (kbd key) (lambda () (interactive) (spawn command)))
      (exwm-input-set-key (kbd key) (lambda () (interactive) (spawn command)))
      (global-set-key (kbd key) (lambda () (interactive) (spawn command))))))

(provide 'mf-exwm-utils)
;;; mf-exwm-utils.el ends here)
