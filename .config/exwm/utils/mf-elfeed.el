;;; mf-elfeed.el --- My mf-elfeed.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;;
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Maintainer: Marcos Felipe <marcos.felipe@tuta.io>
;; Created: janeiro 15, 2022
;; Modified: janeiro 15, 2022
;; Version: 0.0.1
;; Keywords:
;; Package-Requires: ((emacs "24.3"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;
;;
;;; Code:
(setup (:straight elfeed)
  (:with-hook elfeed-search-mode-hook
    (:hook 'elfeed-update))
  (setq elfeed-log-level 'error)
  (setq elfeed-search-filter "@36-months-ago +unread")
  (setq elfeed-search-title-max-width 65)
  (setq elfeed-show-unique-buffers t)
  (setq elfeed-search-date-format '("%d/%b %H:%M" 12 :left)))
(defun elfeed-show--buffer-name (entry)
  "Return the appropriate buffer name for ENTRY.
The result depends on the value of `elfeed-show-unique-buffers'."
  (if elfeed-show-unique-buffers
      (format "%s %s" (elfeed-entry-title entry) (format-time-string "%F" (elfeed-entry-date entry)))
    "*elfeed-entry*"))
(defun none-elfeed (LEVEL FMT &rest OBJECTS))
(advice-add 'elfeed-log :override 'none-elfeed)
;; Start live chat
;; https://www.youtube.com/live_chat?is_popout=1&v=IHeKUeO7bpo
(provide 'mf-elfeed)
;;; mf-elfeed.el ends here
