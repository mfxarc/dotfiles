;;; mf-dir.el --- My file management tools setup -*- lexical-binding: t; -*-
(setup dired
  (:also-straight dired-hide-dotfiles dired-ranger)
  (setq sudo-edit-local-method "su")
  (setq dired-recursive-deletes 'always)
  (setq dired-recursive-copies 'always)
  (setq dired-create-destination-dirs 'always)
  (setq dired-auto-revert-buffer t)
  (setq dired-dwim-target t)
  (setq dired-hide-details-hide-symlink-targets nil)
  ;;
  (setq dired-use-ls-dired nil)
  (setq dired-listing-switches "-vlAh --group-directories-first")
  ;;
  (setq large-file-warning-threshold nil)
  (setq dired-clean-confirm-killing-deleted-buffers nil)
  (setq dired-no-confirm '(byte-compile load chgrp chmod chown copy move hardlink symlink shell touch))
  (setq image-dired-thumb-size 150)
  (setq auto-revert-verbose 'nil)
  ;;
  (add-hook 'dired-mode-hook 'dired-async-mode)
  (add-hook 'dired-mode-hook 'auto-revert-mode)
  (add-hook 'dired-mode-hook 'dired-hide-details-mode))
(require 'dired)

;;; Custom functions

(defun dired-sort ()
  "Sort dired dir listing in different ways.
Prompt for a choice."
  (interactive)
  (let (sort-by arg)
    (setq sort-by (completing-read "Sort by:" '("name" "size" "date" "extension")))
    (pcase sort-by
      ("name" (setq arg "-vlAh --group-directories-first"))
      ("date" (setq arg "-vlAht --group-directories-first"))
      ("size" (setq arg "-vlAhS --group-directories-first"))
      ("extension" (setq arg "-vlahX --group-directories-first"))
      (otherwise (error "Dired-sort: unknown option %s" otherwise)))
    (dired-sort-other arg)))

(defun dired-identify-file ()
  "Identify file extension."
  (interactive)
  (let* ((filename (dired-get-file-for-visit))
         (file (file-name-extension filename)))
    (message file)))

(defun dired-up-directory ()
  "`dired-up-directory' in same buffer."
  (interactive)
  (find-alternate-file ".."))

(defun dired-find-file ()
  "Replace current dired buffer with file buffer or open it externally if needed."
  (interactive)
  (let* ((path (dired-get-file-for-visit))
         (file (file-name-nondirectory path))
         (ext (file-name-extension file)))
    (pcase ext
      ((or "mp4" "mkv" "webm" "opus" "ogg" "mp3" "mov" "mpg" "mpeg" "wmv"
	   "docx" "xlsx" "pdf" "epub" "djvu" "ps"
	   "gif" "png" "jpg" "jpeg" "webp" "svg")
       (message "Opening %s externally" file)
       (call-process-shell-command (format "xdg-open \"%s\" &" path)))
      ((or "bz" "bz2" "tbz" "tbz2" "gz" "tgz" "xz" "tar")
       (progn (message "Extracting %s..." file)
              (call-process-shell-command (format "tar xfv \"%s\" &" path))))
      ((or "zip" "7z" "rar")
       (progn (message "Extracting %s..." file)
              (call-process-shell-command (format "7z x \"%s\" &" path))))
      (_ (find-alternate-file path)))))

(defun dired-copy-path ()
  "Copy selected file/directory path."
  (interactive)
  (let ((path (dired-get-file-for-visit)))
    (kill-new path)
    (message "Copied path - %s" path)))

(defun dired-song-edit ()
  "Edit opus file"
  (interactive)
  (let* ((files (dired-get-marked-files t))
         (genre (completing-read "Genre: " "")))
    ;; (message files)))
    (shell-command (format "ls %s | xargs -L1 opustags -s Genre=\"%s\" -i" (mapconcat 'identity files " ") genre))))

(defun dired-shell-command (command)
  "Run shell command in marked or current files."
  (let ((files (dired-get-marked-files t)))
    (call-process-shell-command
     (format "ls %s | xargs -L1 %s &" (mapconcat 'identity files " ") command))))

(defun dired-kdeconnect-share ()
  "Share marked or current files using kdeconnect."
  (interactive)
  (dired-do-shell-command "kdeconnect-handler"))

(defun sudo-find-file (file-name)
  "Like find file, but opens the file as root."
  (interactive "FSudo find: ")
  (let ((tramp-file-name (concat "/sudo::" (expand-file-name file-name))))
    (find-file tramp-file-name)))

(defun sudo-edit-file ()
  "Edit current file as root."
  (interactive)
  (let ((tramp-file-name (concat "/sudo::" (expand-file-name buffer-file-name))))
    (find-file tramp-file-name)))

(provide 'mf-dir)
