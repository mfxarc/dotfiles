;;; m-blog.el --- My m-blog.el setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: março 09, 2022
;;
;;; Commentary:
;;
;;  My configuration for m-blog.el
;;
;;; Code:

;; TODO publish command
;; TODO hugo serve (preview blog)
;; https://emacstil.com/til/2022/03/13/process-org-files-programmatically/

(setq hugo-blog-url "https://mfxarc.github.io/blog/")
(setq hugo-root-path (expand-file-name "blog/" m-data-dir))
(setq hugo-public-path (expand-file-name "public/" hugo-root-path))
(setq hugo-post-path (expand-file-name "blog/content/posts" m-data-dir))

(defun hugo-new-post (title)
  "New hugo blog post."
  (interactive "sPost title: ")
  (let* ((filename (downcase (replace-regexp-in-string "[[:space:]]+" "_" title)))
	 (fileorg (concat filename ".org"))
	 (filepathorg (expand-file-name fileorg hugo-post-path))
	 (filepath (expand-file-name filename hugo-post-path))
	 (fileother (expand-file-name title hugo-post-path))
	 (date (format-time-string "%Y-%m-%d")))
    (if (or (file-exists-p filepath)
	    (file-exists-p fileother)
	    (file-exists-p filepathorg)
	    (file-exists-p (expand-file-name title hugo-post-path)))
	(find-file filepath)
      (progn (make-empty-file filepathorg)
	     (find-file filepathorg)
	     (insert "#+title: " title "\n#+date: " date "\n#+type: post\n#+tags:"))
      (message filename))))

(defun hugo-update-blog ()
  "Update hugo blog with new posts and changes."
  (interactive)
  (let ((default-directory hugo-root-path))
    (progn (call-process-shell-command "hugo --cleanDestinationDir --minify"))))

(defun hugo-commit-blog ()
  "Commit hugo blog changes."
  (interactive)
  (let ((default-directory hugo-public-path))
    (progn (shell-command "git add ./")
	   (shell-command "git commit -q -m 'Updated blog'")
	   (message "Changes commited to blog"))))

(defun hugo-push-blog ()
  "Push hugo blog changes."
  (interactive)
  (let ((default-directory hugo-public-path))
    (magit-push-current "origin/main" nil)))

(defun hugo-lazy-update ()
  "Update, commit and push changes to blog."
  (interactive)
  (progn (hugo-update-blog)
	 (hugo-commit-blog)
	 (hugo-push-blog)))

(defun hugo-open-blog ()
  "Open blog in browser."
  (interactive)
  (browse-url hugo-blog-url)
  (message "Opening blog in browser..."))

(defun hugo-find-post ()
  "List blog posts."
  (interactive)
  (hugo-new-post
   (completing-read "Select blog post: " (directory-files hugo-post-path nil ".org" nil nil))))

(defun hugo-get-post-title ()
  "List blog posts."
  (interactive)
  (let* ((file (completing-read "Delete blog post: " (directory-files hugo-post-path nil ".org" nil nil)))
	 (filepath (expand-file-name file hugo-post-path)))
    (org-file-get-title filepath)))

(defun hugo-delete-post ()
  "Delete hugo blog post."
  (interactive)
  (let* ((file (completing-read "Delete blog post: " (directory-files hugo-post-path nil ".org" nil nil)))
	 (filepath (expand-file-name file hugo-post-path)))
    (when (yes-or-no-p (format "Really want to delete this post?: %s" file))
      (progn (delete-file filepath)
	     (message "Post delete: %s" file)))))

(defun hugo-magit ()
  "Open magit-status in blog directory."
  (interactive)
  (let ((default-directory hugo-public-path))
    (magit-status-here)))

(defun hugo-ripgrep ()
  "Ripgrep in blog posts directory."
  (interactive)
  (consult-ripgrep hugo-post-path))

;; https://www.reddit.com/r/emacs/comments/f3o0v8/anyone_have_good_examples_for_transient/
(define-transient-command hugo-management ()
  "Manage your hugo blog."
  [
   ["Manage posts"
    ("n" "Create new post" hugo-new-post)
    ("f" "Find or create post" hugo-find-post)
    ("d" "Delete existent post" hugo-delete-post)
    ("r" "Ripgrep in posts directory" hugo-ripgrep)
    ]
   ["Manage repository"
    ("u" "Update public directory" hugo-update-blog)
    ("c" "Commit changes to repository" hugo-commit-blog)
    ("p" "Push changes to repository" hugo-push-blog)
    ]
   [
    ""
    ("l" "Update, Commit and Push automatically" hugo-lazy-update)
    ("s" "Open magit-status" hugo-magit)
    ("o" "Open blog in browser" hugo-open-blog)
    ]
   ])


(provide 'mf-blog)
;;;m-blog.el ends here
