;;; m-youtube.el --- My YouTube setup -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 18, 2022
;;
;;; Commentary:
;;
;;  My configuration for m-youtube.el
;;
;;  DONE - Script to fetch data
;;  DONE - Channel's view
;;  DONE - Copy video url
;;  TODO - Multiple selection of videos to mpv-playlist
;;  TODO - Playlist system like bookmarks separated by sections
;;  TODO - Popup thumbnail
;;
;;  https://emacs.stackexchange.com/questions/19877/how-to-evaluate-elisp-code-contained-in-a-string
;;
;;; Code:

;; Utils to use across the code
(defun eval-string (string)
  "Eval 'STRING' as elisp expression."
  (eval (car (read-from-string (format "(progn %s)" string)))))

(defun youtube--fetch-data (string)
  "Fetch youtube data of 'STRING'."
  (shell-command-to-string (format "yt-fetch %s" string)))

(defun youtube--fetch-desc (string)
  "Fetch youtube description of 'STRING'."
  (shell-command-to-string (format "yt-desc %s" string)))

(defun youtube--get-channel (string)
  "Fetch youtube channel of 'STRING'."
  (shell-command-to-string (format "yt-channel %s" string)))

(defun youtube--get-id ()
  "Get the 'videoId' of the current entry."
  (prin1-to-string (aref (tabulated-list-get-entry) 0)))

(defun youtube--get-title ()
  "Get the 'title' of the current entry."
  (prin1-to-string (aref (tabulated-list-get-entry) 1)))
;; -----------------------------------------------------------------------------------------
;; The essential
(define-derived-mode youtube-search-mode tabulated-list-mode "youtube-search-mode"
  "Major mode for tabulated list of 'youtube-search'."
  (require 'tablist)
  (setq-local tabulated-list-padding 1)
  (setq-local tabulated-list-sort-key (cons "Title" nil))
  (tablist-minor-mode)
  (tabulated-list-init-header)
  (tabulated-list-print t)
  (goto-char 2))

(define-derived-mode youtube-channel-mode youtube-search-mode "youtube-channel-mode"
  "Major mode for tabulated list of 'youtube-channel'.")

(define-derived-mode youtube-desc-mode org-mode "youtube-desc-mode"
  "Major mode for youtube entry description."
  (read-only-mode)
  (goto-char 1))

(defun youtube-search ()
  "YouTube buffer."
  (interactive)
  (let ((buffer (get-buffer-create "youtube-search")))
    (with-current-buffer buffer
      (setq-local tabulated-list-format [("ID" 11 t) ("Title" 75 nil) ("Channel" 10 t)])
      (setq-local tabulated-list-entries (eval-string (youtube--fetch-data (read-string "What do you want to see?: "))))
      (youtube-search-mode))
    (display-buffer buffer '(display-buffer-same-window . nil))))

;; -----------------------------------------------------------------------------------------
;; Derivaded functionalities
(defun youtube-search-open ()
  "Open current youtube entry in mpv."
  (interactive)
  (let* ((link (aref (tabulated-list-get-entry) 0))
	 (title (aref (tabulated-list-get-entry) 1)))
    (when (call-process-shell-command (format "mpv-tube \"https://www.youtube.com/watch?v=%s\" &" link))
      (message "Opening %s..." title))))

(defun youtube-search-desc ()
  "Open current youtube entry description."
  (interactive)
  (let ((buffer (generate-new-buffer (youtube--get-title)))
	(id (youtube--get-id)))
    (with-current-buffer buffer
      (insert (youtube--fetch-desc id))
      (youtube-desc-mode))
    (display-buffer buffer '(display-buffer-same-window . nil))))

(defun youtube-search-channel ()
  "Open current youtube entry channel."
  (interactive)
  (let* ((channel (prin1-to-string (aref (tabulated-list-get-entry) 2)))
	 (id (prin1-to-string (aref (tabulated-list-get-entry) 0)))
	 (buffer (get-buffer-create channel)))
    (with-current-buffer buffer
      (setq-local tabulated-list-format [("ID" 11 t) ("Title" 75 nil) ("Date" 10 t)])
      (setq-local tabulated-list-entries (eval-string (youtube--get-channel id)))
      (youtube-channel-mode))
    (display-buffer buffer '(display-buffer-same-window . nil))))

(defun youtube-search-open-in-browser ()
  "Open youtube-search entry in browser."
  (interactive)
  (let ((url (concat "https://invidious.snopyta.org/watch?v=" (aref (tabulated-list-get-entry) 0))))
    (message "Opening url: %s" url)
    (call-process-shell-command (format "qutebrowser \"%s\" &" url))))

(defun youtube-search-copy-url ()
  "Copy youtube-search entry url."
  (interactive)
  (let ((url (concat "https://invidious.snopyta.org/watch?v=" (aref (tabulated-list-get-entry) 0))))
    (message "URL copied: %s" url)
    (kill-new url)))

(provide 'mf-youtube)
;;; m-youtube.el ends here
