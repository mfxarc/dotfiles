;;; m-internet.el -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Marcos Felipe
;; Author: Marcos Felipe <https://github.com/mfxarc>
;; Created: fevereiro 17, 2022
;;
;;; Commentary:
;;  
;;  My configuration for m-internet.el
;;  
;;; Code:

(defun browse-url-program (url &optional _new-window)
  "Load URL in default browser."
  (interactive (browse-url-interactive-arg "URL: "))
  (setq url (browse-url-encode-url url))
  (let* ((process-environment (browse-url-process-environment)))
    (cond ((or (string-match "\\(invidious\\)" url) (string-match "\\(youtube\\)" url))
           (call-process-shell-command (format "mpv-tube \"%s\" &" url)))
          (t (call-process-shell-command (format "browser \"%s\" &" url))))))

(setq browse-url-browser-function #'browse-url-program)

(provide 'mf-internet)
;;; m-internet.el ends here
