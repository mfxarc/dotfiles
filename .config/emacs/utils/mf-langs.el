;;; m-langs.el --- My langs setup -*- lexical-binding: t; -*-
(setup (:elpa haskell-mode)
  (:file-match "\\.hs\\'"))
(setup (:elpa clojure-mode)
  (:file-match "\\.clj\\'"))
(setup (:elpa flymake-lua))
(setup (:elpa lua-mode)
  (:file-match "\\.lua\\'")
  (:hook 'flymake-lua-load)
  (setq lua-indent-level 2))
(setup (:elpa json-mode)
  (:file-match "\\.json\\'")
  (:hook 'json-mode-beautify))
(setup css-mode
  (:file-match "\\.css\\'" "\\.rasi\\'"))
(setup emacs-lisp-mode
  (:elpa aggressive-indent)
  (:hook 'aggressive-indent-mode)
  (:hook 'outline-minor-mode)
  (:hook 'hs-minor-mode)
  (global-eldoc-mode -1)
  (:hook eldoc-mode))

(push '("ui\\'" . xml-mode) auto-mode-alist)
(push '("conky.conf" . lua-mode) auto-mode-alist)
(push '("list\\'" . conf-mode) auto-mode-alist)
(push '("rc\\'" . conf-mode) auto-mode-alist)
(push '("xresources" . conf-mode) auto-mode-alist)
(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(setup (:elpa flymake-shellcheck)
  (add-hook 'sh-mode-hook 'flymake-shellcheck-load)
  (add-hook 'sh-mode-hook 'flymake-mode))

(setq compilation-always-kill t)
(setq compilation-ask-about-save nil)
(setq compilation-scroll-output t)

(defun shell-save-hook ()
  "Ensure that shell script files use correct major-mode."
  (when (save-excursion (goto-char (point-min)) (looking-at "^#!/")) (shell-script-mode)))
(add-hook 'after-save-hook 'shell-save-hook)

(provide 'mf-langs)
