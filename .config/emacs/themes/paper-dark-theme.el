;;; paper-dark-theme.el -*- lexical-binding: t; no-byte-compile: t; -*-

(deftheme paper-dark "Paper based theme.")
;;; Color Palette
;; #AD8B73
;; #CFC1B5
;; #2274A5
(defvar paper-dark-colors-alist
  '(("scuro"           . "#282828")
    ("buio"            . "#1F1F1F")
    ("olive"           . "#33312F")
    ("verde"           . "#B9BAA2")
    ("rosso"           . "#835549")
    ("marrone"         . "#7C6F64")
    ("chiaro"          . "#A89984"))
  "List of Paper colors.
Each element has the form (NAME . HEX). ")
(defmacro paper-dark/with-color-variables (&rest body)
  "`let' bind all colors defined in `paper-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
	 ,@(mapcar (lambda (cons) (list (intern (car cons)) (cdr cons))) paper-dark-colors-alist)) ,@body))
;; Setting special fonts
(set-fontset-font t 'hangul '("Sarasa Mono K" . "unicode-bmp"))
(set-fontset-font t 'symbol '("Symbola" . "unicode-bmp"))
;;; Theme Faces
(paper-dark/with-color-variables
 (custom-theme-set-faces
  'paper-dark
;;;; Built-in
  '(button ((t (:underline t))))
  ;; Defaults
  `(default ((t (:foreground ,chiaro :background ,scuro))))
  `(fixed-pitch ((t (:inherit default))))
  `(variable-pitch ((t (:inherit default))))
  `(italic ((t (:inherit variable-pitch :slant italic))))
  `(solaire-default-face ((t (:background ,buio))))
  ;; Windows
  `(fringe ((t (:inherit default :background nil))))
  `(window-divider ((t (:foreground ,olive))))
  `(window-divider-first-pixel ((t (:inherit window-divider))))
  `(window-divider-last-pixel ((t (:inherit window-divider))))
  ;; Completion
  `(completions-annotations ((t (:inherit default :underline nil :slant italic :foreground ,chiaro))))
  `(highlight ((t (:background ,olive))))
  `(vertico-current ((t (:background ,olive))))
  ;; Elfeed
  `(elfeed-search-feed-face ((t (:foreground ,chiaro))))
  `(elfeed-search-tag-face ((t (:inherit font-lock-variable-name-face))))
  `(elfeed-search-date-face ((t (:inherit font-lock-comment-face))))
  `(elfeed-search-title-face ((t (:foreground ,marrone))))
  `(elfeed-search-unread-title-face ((t (:background nil :foreground ,chiaro))))
  `(message-header-name ((t (:inherit org-document-info-keyword))))
  `(message-header-subject ((t (:inherit org-document-title))))
  `(message-header-to ((t (:inherit org-document-title))))
  `(message-header-other ((t (:inherit org-document-info))))
  ;; Term
  `(term-color-red ((t (:foreground "#F89169"))))
  `(term-color-green ((t (:foreground "#689D6A"))))
  `(term-color-blue ((t (:foreground "#63A6A5"))))
  ;; Org
  `(org-block ((t (:inherit default :background ,buio))))
  `(org-verbatim ((t (:inherit default :foreground ,chiaro :slant italic :box (:line-width 1 :color ,chiaro)))))
  `(org-document-title ((t (:foreground ,chiaro))))
  `(org-document-info ((t (:foreground ,chiaro))))
  `(org-document-info-keyword ((t (:inherit org-document-info))))
  `(org-property-value ((t (:inherit org-document-info))))
  `(org-special-keyword ((t (:inherit org-document-info))))
  `(org-meta-line ((t (:inherit org-document-info-keyword))))
  `(org-drawer ((t (:inherit org-document-info-keyword))))
  `(org-indent ((t (:inherit (org-hide default)))))
  `(org-level-1 ((t (:foreground ,chiaro :weight bold :height 1.0))))
  `(org-level-1 ((t (:height 1.0 :weight bold ))))
  `(org-level-2 ((t (:height 1.0 :weight bold))))
  `(org-level-3 ((t (:height 1.0 :weight bold))))
  `(org-level-4 ((t (:height 1.0 :weight bold))))
  `(org-level-5 ((t (:height 1.0 :weight bold))))
  `(org-level-6 ((t (:height 1.0 :weight bold))))
  `(org-level-7 ((t (:height 1.0 :weight bold))))
  `(org-level-8 ((t (:height 1.0 :weight bold))))
  `(org-agenda-date-today ((t (:foreground ,chiaro :slant italic :weight bold))) t)
  `(org-agenda-structure ((t (:inherit font-lock-comment-face))))
  `(org-archived ((t (:foreground ,scuro :weight bold))))
  `(org-checkbox ((t (:foreground ,chiaro))))
  `(org-date ((t (:foreground ,chiaro :underline t))))
  `(org-deadline-announce ((t (:foreground ,scuro))))
  `(org-done ((t (:bold t :weight bold :foreground ,marrone))))
  `(org-formula ((t (:foreground ,scuro))))
  `(org-headline-done ((t (:foreground ,marrone))))
  `(org-hide ((t (:foreground ,scuro))))
  `(org-link ((t (:inherit link))))
  `(org-scheduled ((t (:foreground ,scuro))))
  `(org-special-keyword ((t (:inherit font-lock-comment-face))))
  `(org-table ((t (:foreground ,chiaro))))
  `(org-tag ((t (:bold t :weight bold))))
  `(org-todo ((t (:bold t :foreground ,scuro :weight bold))))
  `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))
  `(org-warning ((t (:bold t :foreground ,scuro :weight bold :underline nil))))
  `(org-footnote ((t (:foreground ,scuro :weight bold))))
  ;; Magit
  `(diff-added ((t (:foreground ,scuro :background ,verde))))
  `(diff-removed ((t (:foreground ,scuro :background ,rosso))))
  `(diff-refine-removed ((t (:foreground ,chiaro :background ,scuro))))
  `(diff-changed ((t (:foreground ,scuro))))
  `(diff-context ((t (:foreground ,chiaro))))
  `(diff-refine-added ((t)))
  `(diff-refine-change ((t :inherit diff-changed :weight bold)))
  `(diff-header ((,class (:foreground ,scuro :weight bold))
		 (t (:foreground ,scuro :weight bold))))
  `(diff-file-header
    ((,class (:foreground ,scuro :weight bold))
     (t (:foreground ,scuro :weight bold))))
  `(diff-hunk-header
    ((,class (:foreground ,scuro :weight bold))
     (t (:foreground ,scuro :weight bold))))
  `(diff-hl-insert ((t (:foreground ,scuro :background ,scuro))))
  `(diff-hl-delete ((t (:foreground ,scuro :background ,scuro))))
  `(diff-hl-change ((t (:foreground ,scuro :background ,scuro))))

  `(magit-diff-context ((t (:inherit default :foreground ,olive))))
  `(magit-diff-context-highlight ((t (:inherit default :foreground ,olive))))
  `(magit-diff-removed-highlight ((t (:inherit diff-removed))))
  `(magit-diff-added-highlight ((t (:inherit diff-added))))
  `(magit-diff-removed ((t (:inherit diff-removed))))
  `(magit-diff-added ((t (:inherit diff-added))))

  `(magit-diff-file-heading ((t (:inherit default))))
  `(magit-diff-file-heading-highlight ((t (:inherit hl-line))))
  `(magit-diff-hunk-heading ((t (:inherit hl-line))))
  `(magit-diff-hunk-heading-highlight ((t (:inherit hl-line))))
  `(magit-section-heading ((t (:inherit org-level-1))))
  `(magit-section-highlight ((t (:inherit hl-line))))

  `(magit-branch-remote ((t (:inherit font-lock-constant-face))))
  `(magit-branch-local ((t (:inherit font-lock-constant-face))))
  ;; EWW
  `(eww-valid-certificate ((t (:inherit default))))
  `(eww-form-text ((t (:inherit default :box (:line-width 1)))))
  `(eww-form-file ((t (:inherit eww-form-text))))
  `(eww-form-select ((t (:inherit eww-form-text))))
  `(eww-form-submit ((t (:inherit eww-form-text))))
  `(eww-form-checkbox ((t (:inherit eww-form-text))))
  `(eww-form-textarea ((t (:inherit eww-form-text))))
  ;; Tab-bar
  `(tab-bar ((t (:foreground ,chiaro :background ,scuro))))
  `(tab-bar-tab ((t (:foreground ,chiaro :background ,olive))))
  ;; LaTeX
  `(font-latex-sectioning-0-face ((t (:inherit org-level-1))))
  `(font-latex-sectioning-1-face ((t (:inherit org-level-2))))
  `(font-latex-sectioning-2-face ((t (:inherit org-level-3))))
  `(font-latex-sectioning-3-face ((t (:inherit org-level-4))))
  `(font-latex-sectioning-4-face ((t (:inherit org-level-5))))
  `(font-latex-sectioning-5-face ((t (:inherit org-level-6))))
  `(font-latex-italic-face ((t (:inherit italic))))
  `(font-latex-bold-face ((t (:weight bold))))
  `(font-latex-warning-face ((t (:inherit warning))))
  ;; Hydra
  `(hydra-face-red ((t (:foreground ,rosso))))
  `(hydra-face-blue ((t (:foreground ,rosso))))
  `(hydra-face-pink ((t (:foreground ,rosso))))
  ;; Hydra
  `(shadow ((t (:foreground ,chiaro))))
  `(link ((t (:foreground ,rosso :underline t))))
  `(link-visited ((t (:underline t))))
  `(cursor ((t (:foreground ,scuro :background ,scuro))))
  `(escape-glyph ((t (:foreground ,scuro :bold t))))
  `(header-line ((t (:inherit mode-line))))
  `(success ((t (:foreground ,scuro :weight bold))))
  `(warning ((t (:foreground ,scuro :background ,marrone :weight bold))))
;;;;; compilation
  `(compilation-column-face ((t (:foreground ,scuro))))
  `(compilation-enter-directory-face ((t (:foreground ,scuro))))
  `(compilation-error-face ((t (:foreground ,rosso :weight bold :underline t))))
  `(compilation-face ((t (:foreground ,chiaro))))
  `(compilation-info-face ((t (:foreground ,chiaro))))
  `(compilation-info ((t (:foreground ,chiaro :underline t))))
  `(compilation-leave-directory-face ((t (:foreground ,scuro))))
  `(compilation-line-face ((t (:foreground ,scuro))))
  `(compilation-line-number ((t (:foreground ,scuro))))
  `(compilation-message-face ((t (:foreground ,scuro))))
  `(compilation-warning-face ((t (:foreground ,scuro :weight bold :underline t))))
  `(compilation-mode-line-exit ((t (:foreground ,scuro :weight bold))))
  `(compilation-mode-line-fail ((t (:foreground ,scuro :weight bold))))
  `(compilation-mode-line-run ((t (:foreground ,scuro :weight bold))))
;;;;; grep
  `(grep-context-face ((t (:foreground ,scuro))))
  `(grep-error-face ((t (:foreground ,scuro :weight bold :underline t))))
  `(grep-hit-face ((t (:foreground ,scuro :weight bold))))
  `(grep-match-face ((t (:foreground ,scuro :weight bold))))
  `(match ((t (:background ,chiaro :foreground ,scuro))))
;;;;; isearch
  `(isearch ((t (:foreground ,chiaro :weight bold :background ,scuro))))
  `(isearch-fail ((t (:foreground ,chiaro :weight bold :background ,scuro))))
  `(lazy-highlight ((t (:foreground ,chiaro :weight bold :background ,scuro))))

  `(menu ((t (:foreground ,scuro :background ,chiaro))))
  `(minibuffer-prompt ((t (:foreground ,chiaro))))
  `(region ((,class (:background ,chiaro :foreground ,scuro))))
  `(secondary-selection ((t (:background ,scuro))))
  `(trailing-whitespace ((t (:background ,scuro))))
  `(vertical-border ((t (:foreground ,chiaro))))
;;;;; font lock
  `(font-lock-builtin-face ((t (:foreground ,rosso))))
  `(font-lock-comment-face ((t (:foreground ,marrone :slant italic))))
  `(sh-heredoc ((t (:inherit font-lock-comment-face))))
  `(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face))))
  `(font-lock-constant-face ((t (:foreground ,rosso))))
  `(font-lock-doc-face ((t (:inherit font-lock-comment-face))))
  `(font-lock-function-name-face ((t (:foreground ,rosso :weight normal))))
  `(font-lock-keyword-face ((t (:foreground ,rosso))))
  `(font-lock-negation-char-face ((t (:foreground ,rosso :weight normal))))
  `(font-lock-preprocessor-face ((t (:foreground ,rosso :weight normal))))
  `(font-lock-regexp-grouping-construct ((t (:foreground ,rosso :weight normal))))
  `(font-lock-regexp-grouping-backslash ((t (:foreground ,rosso :weight normal))))
  `(font-lock-string-face ((t (:foreground ,rosso))))
  `(font-lock-type-face ((t (:foreground ,rosso))))
  `(font-lock-variable-name-face ((t (:foreground ,rosso))))
  `(font-lock-warning-face ((t (:foreground ,rosso :weight normal))))
  `(c-annotation-face ((t (:inherit font-lock-constant-face))))
;;;;; ledger
  `(ledger-font-directive-face ((t (:foreground ,scuro))))
  `(ledger-font-periodic-xact-face ((t (:inherit ledger-font-directive-face))))
  `(ledger-occur-xact-face ((t (:background ,chiaro))))
;;;; Third-party
;;;;; ace-jump
  `(ace-jump-face-background
    ((t (:foreground ,chiaro :background ,chiaro :inverse-video nil))))
  `(ace-jump-face-foreground
    ((t (:foreground ,scuro :background ,chiaro :inverse-video nil))))
;;;;; anzu
  `(anzu-mode-line ((t (:foreground ,scuro :weight bold))))
;;;;; auto-complete
  `(ac-candidate-face ((t (:background ,chiaro :foreground ,scuro))))
  `(ac-selection-face ((t (:background ,chiaro :foreground ,scuro))))
  `(popup-tip-face ((t (:background ,scuro :foreground ,scuro))))
  `(popup-scroll-bar-foreground-face ((t (:background ,chiaro))))
  `(popup-scroll-bar-background-face ((t (:background ,chiaro))))
  `(popup-isearch-match ((t (:background ,chiaro :foreground ,scuro))))
;;;;; clojure-test-mode
  `(clojure-test-failure-face ((t (:foreground ,scuro :weight bold :underline t))))
  `(clojure-test-error-face ((t (:foreground ,scuro :weight bold :underline t))))
  `(clojure-test-success-face ((t (:foreground ,scuro :weight bold :underline t))))
;;;;; dired/dired+/dired-subtree
  `(diredp-display-msg ((t (:foreground ,scuro))))
  `(diredp-compressed-file-suffix ((t (:foreground ,scuro))))
  `(diredp-date-time ((t (:foreground ,scuro))))
  `(diredp-deletion ((t (:foreground ,scuro))))
  `(diredp-deletion-file-name ((t (:foreground ,scuro))))
  `(diredp-dir-heading ((t (:foreground ,scuro :background ,chiaro :weight bold))))
  `(diredp-dir-priv ((t (:foreground ,scuro))))
  `(diredp-exec-priv ((t (:foreground ,scuro))))
  `(diredp-executable-tag ((t (:foreground ,scuro))))
  `(diredp-file-name ((t (:foreground ,scuro))))
  `(diredp-file-suffix ((t (:foreground ,scuro))))
  `(diredp-flag-mark ((t (:foreground ,scuro))))
  `(diredp-flag-mark-line ((t (:foreground ,scuro))))
  `(diredp-ignored-file-name ((t (:foreground ,chiaro))))
  `(diredp-link-priv ((t (:foreground ,scuro))))
  `(diredp-mode-line-flagged ((t (:foreground ,scuro))))
  `(diredp-mode-line-marked ((t (:foreground ,scuro))))
  `(diredp-no-priv ((t (:foreground ,scuro))))
  `(diredp-number ((t (:foreground ,scuro))))
  `(diredp-other-priv ((t (:foreground ,scuro))))
  `(diredp-rare-priv ((t (:foreground ,scuro))))
  `(diredp-read-priv ((t (:foreground ,scuro))))
  `(diredp-symlink ((t (:foreground ,chiaro :background ,scuro))))
  `(diredp-write-priv ((t (:foreground ,scuro))))
  `(dired-subtree-depth-1-face ((t (:background ,chiaro))))
  `(dired-subtree-depth-2-face ((t (:background ,chiaro))))
  `(dired-subtree-depth-3-face ((t (:background ,chiaro))))
;;;;; ediff
  `(ediff-current-diff-A ((t (:foreground ,scuro :background ,scuro))))
  `(ediff-current-diff-Ancestor ((t (:foreground ,scuro :background ,scuro))))
  `(ediff-current-diff-B ((t (:foreground ,scuro :background ,scuro))))
  `(ediff-current-diff-C ((t (:foreground ,scuro :background ,scuro))))
  `(ediff-even-diff-A ((t (:background ,chiaro))))
  `(ediff-even-diff-Ancestor ((t (:background ,chiaro))))
  `(ediff-even-diff-B ((t (:background ,chiaro))))
  `(ediff-even-diff-C ((t (:background ,chiaro))))
  `(ediff-fine-diff-A ((t (:foreground ,scuro :background ,scuro :weight bold))))
  `(ediff-fine-diff-Ancestor ((t (:foreground ,scuro :background ,scuro weight bold))))
  `(ediff-fine-diff-B ((t (:foreground ,scuro :background ,scuro :weight bold))))
  `(ediff-fine-diff-C ((t (:foreground ,scuro :background ,scuro :weight bold ))))
  `(ediff-odd-diff-A ((t (:background ,chiaro))))
  `(ediff-odd-diff-Ancestor ((t (:background ,chiaro))))
  `(ediff-odd-diff-B ((t (:background ,chiaro))))
  `(ediff-odd-diff-C ((t (:background ,chiaro))))
;;;;; eshell
  `(eshell-prompt ((t (:foreground ,scuro :weight bold))))
  `(eshell-ls-archive ((t (:foreground ,scuro :weight bold))))
  `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
  `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
  `(eshell-ls-directory ((t (:foreground ,scuro :weight bold))))
  `(eshell-ls-executable ((t (:foreground ,scuro))))
  `(eshell-ls-unreadable ((t (:foreground ,chiaro))))
  `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
  `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
  `(eshell-ls-special ((t (:foreground ,scuro :weight bold))))
  `(eshell-ls-symlink ((t (:foreground ,chiaro :background ,scuro))))
;;;;; evil
  `(evil-search-highlight-persist-highlight-face ((t (:inherit lazy-highlight))))
;;;;; flx
  `(flx-highlight-face ((t (:foreground ,scuro :weight bold))))
;;;;; flycheck
  `(flycheck-error
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro) :inherit unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
  `(flycheck-warning
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro) :inherit unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
  `(flycheck-info
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro) :inherit unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
  `(flycheck-fringe-error ((t (:foreground ,scuro :weight bold))))
  `(flycheck-fringe-warning ((t (:foreground ,scuro :weight bold))))
  `(flycheck-fringe-info ((t (:foreground ,scuro :weight bold))))
;;;;; flymake
  `(flymake-errline
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)
		  :inherit unspecified :foreground unspecified :background unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
  `(flymake-warnline
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)
		  :inherit unspecified :foreground unspecified :background unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
  `(flymake-infoline
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)
		  :inherit unspecified :foreground unspecified :background unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
;;;;; flyspell
  `(flyspell-duplicate
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro) :inherit unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
  `(flyspell-incorrect
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro) :inherit unspecified))
     (t (:foreground ,scuro :weight bold :underline t))))
;;;;; git-gutter
  `(git-gutter:added ((t (:foreground ,scuro :weight bold))))
  `(git-gutter:deleted ((t (:foreground ,scuro :weight bold))))
  `(git-gutter:modified ((t (:foreground ,scuro :weight bold))))
  `(git-gutter:unchanged ((t (:foreground ,scuro :weight bold))))
;;;;; git-gutter-fr
  `(git-gutter-fr:added ((t (:foreground ,scuro  :weight bold))))
  `(git-gutter-fr:deleted ((t (:foreground ,scuro :weight bold))))
  `(git-gutter-fr:modified ((t (:foreground ,scuro :weight bold))))
;;;;; guide-key
  `(guide-key/highlight-command-face ((t (:foreground ,scuro))))
  `(guide-key/key-face ((t (:foreground ,scuro))))
  `(guide-key/prefix-command-face ((t (:foreground ,scuro))))
;;;;; highlight-symbol
  `(highlight-symbol-face ((t (:background "gray88" :underline t))))
;;;;; hl-line-mode
  `(hl-line ((t (:background ,olive))))
;;;;; hl-sexp
  `(hl-sexp-face ((,class (:background ,chiaro)) (t :weight bold)))
;;;;; ido-mode
  `(ido-first-match ((t (:foreground ,scuro :weight bold))))
  `(ido-only-match ((t (:foreground ,scuro :weight bold))))
  `(ido-subdir ((t (:foreground ,scuro))))
  `(ido-indicator ((t (:foreground ,scuro :background ,scuro))))
;;;;; indent-guide
  `(indent-guide-face ((t (:foreground ,chiaro))))
;;;;; js2-mode
  `(js2-warning ((t (:underline ,scuro))))
  `(js2-error ((t (:foreground ,scuro :weight bold))))
  `(js2-jsdoc-tag ((t (:foreground ,scuro))))
  `(js2-jsdoc-type ((t (:foreground ,scuro))))
  `(js2-jsdoc-value ((t (:foreground ,scuro))))
  `(js2-function-param ((t (:foreground ,scuro))))
  `(js2-external-variable ((t (:foreground ,scuro))))
;;;;; linum-mode
  `(linum ((t (:foreground ,scuro :background ,chiaro))))
;;;;; magit
  `(magit-header ((t (:foreground ,scuro :background nil :weight bold))))
  `(magit-section-title ((t (:foreground ,scuro :background nil :weight bold))))
  `(magit-branch ((t (:foreground ,scuro :background ,scuro
				  :weight bold
				  :box (:line-width 1 :color ,scuro)))))
  `(magit-item-highlight ((t (:background ,chiaro))))
  `(magit-log-author ((t (:foreground ,scuro))))
  `(magit-log-sha1 ((t (:foreground ,scuro :weight bold))))
  `(magit-tag ((t (:foreground ,scuro :weight bold))))
  `(magit-log-head-label-head ((t (:foreground ,scuro :background ,scuro
					       :weight bold
					       :box (:line-width 1 :color ,scuro)))))
  `(magit-log-head-label-local ((t (:foreground ,scuro :background ,scuro
						:weight bold
						:box (:line-width 1 :color ,scuro)))))
  `(magit-log-head-label-default ((t (:foreground ,scuro :background ,scuro
						  :weight bold
						  :box (:line-width 1 :color ,scuro)))))
  `(magit-log-head-label-remote ((t (:foreground ,scuro :background ,scuro
						 :weight bold
						 :box (:line-width 1 :color ,scuro)))))
  `(magit-log-head-label-tags ((t (:foreground ,scuro :weight bold))))
;;;;; outline
  `(outline-1 ((t (:foreground ,chiaro))))
  `(outline-2 ((t (:foreground ,chiaro))))
  `(outline-3 ((t (:foreground ,chiaro))))
  `(outline-4 ((t (:foreground ,chiaro))))
  `(outline-5 ((t (:foreground ,chiaro))))
  `(outline-6 ((t (:foreground ,chiaro))))
;;;;; rainbow-delimiters
  `(rainbow-delimiters-depth-1-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-2-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-3-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-4-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-5-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-6-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-7-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-8-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-9-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-10-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-11-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-12-face ((t (:foreground ,chiaro))))
;;;;; structured-haskell-mode
  `(shm-current-face ((t (:background ,chiaro))))
  `(shm-quarantine-face ((t (:inherit font-lock-error))))
;;;;; show-paren
  `(show-paren-mismatch ((t (:foreground ,scuro :background ,scuro :weight bold))))
  `(show-paren-match ((t (:foreground ,chiaro :background ,scuro :weight bold))))
;;;;; mode-line
  `(mode-line ((,class (:foreground ,chiaro :background ,scuro :font "Noto Sans" :height 92))))
  `(doom-modeline-bar ((t (:foreground nil :background nil))))
  `(doom-modeline-panel ((t (:foreground nil :background nil))))
  `(doom-modeline-bar-inactive ((t (:foreground nil :background nil))))
  `(mode-line-inactive ((t (:inherit mode-line :foreground ,chiaro))))
  `(mode-line-buffer-id ((t (:foreground ,chiaro))))
  `(header-line ((,class (:inherit mode-line))))
  `(persp-selected-face ((t (:foreground ,scuro :weight light :height 92))))
;;;;; SLIME
  `(slime-repl-output-face ((t (:foreground ,scuro))))
  `(slime-repl-inputed-output-face ((t (:foreground ,scuro))))
  `(slime-error-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)))
     (t
      (:underline ,scuro))))
  `(slime-warning-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)))
     (t
      (:underline ,scuro))))
  `(slime-style-warning-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)))
     (t
      (:underline ,scuro))))
  `(slime-note-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,scuro)))
     (t
      (:underline ,scuro))))
  `(slime-highlight-face ((t (:inherit ,scuro))))
;;;;; web-mode
  `(web-mode-builtin-face ((t (:inherit ,font-lock-builtin-face))))
  `(web-mode-comment-face ((t (:inherit ,font-lock-comment-face))))
  `(web-mode-constant-face ((t (:inherit ,font-lock-constant-face))))
  `(web-mode-css-at-rule-face ((t (:foreground ,scuro ))))
  `(web-mode-css-prop-face ((t (:foreground ,scuro))))
  `(web-mode-css-pseudo-class-face ((t (:foreground ,scuro :weight bold))))
  `(web-mode-css-rule-face ((t (:foreground ,scuro))))
  `(web-mode-doctype-face ((t (:inherit ,font-lock-comment-face))))
  `(web-mode-folded-face ((t (:underline t))))
  `(web-mode-function-name-face ((t (:foreground ,scuro :weight bold))))
  `(web-mode-html-attr-name-face ((t (:foreground ,scuro))))
  `(web-mode-html-attr-value-face ((t (:inherit ,font-lock-string-face))))
  `(web-mode-html-tag-face ((t (:foreground ,scuro :weight bold))))
  `(web-mode-keyword-face ((t (:inherit ,font-lock-keyword-face))))
  `(web-mode-preprocessor-face ((t (:inherit ,font-lock-preprocessor-face))))
  `(web-mode-string-face ((t (:inherit ,font-lock-string-face))))
  `(web-mode-type-face ((t (:inherit ,font-lock-type-face))))
  `(web-mode-variable-name-face ((t (:inherit ,font-lock-variable-name-face))))
  `(web-mode-server-background-face ((t (:background ,chiaro))))
  `(web-mode-server-comment-face ((t (:inherit web-mode-comment-face))))
  `(web-mode-server-string-face ((t (:foreground ,scuro))))
  `(web-mode-symbol-face ((t (:inherit font-lock-constant-face))))
  `(web-mode-warning-face ((t (:inherit font-lock-warning-face))))
  `(web-mode-whitespaces-face ((t (:background ,scuro))))
  `(web-mode-block-face ((t (:background "gray88"))))
  `(web-mode-current-element-highlight-face ((t (:inverse-video t))))
;;;;; whitespace-mode
  `(whitespace-space ((t (:background ,chiaro :foreground ,scuro))))
  `(whitespace-hspace ((t (:background ,chiaro :foreground ,scuro))))
  `(whitespace-tab ((t (:background ,scuro))))
  `(whitespace-newline ((t (:foreground ,scuro))))
  `(whitespace-trailing ((t (:background ,scuro))))
  `(whitespace-line ((t (:background nil :foreground ,scuro))))
  `(whitespace-space-before-tab ((t (:background ,chiaro :foreground ,scuro))))
  `(whitespace-indentation ((t (:background ,chiaro :foreground ,scuro))))
  `(whitespace-empty ((t (:background ,scuro))))
  `(whitespace-space-after-tab ((t (:background ,chiaro :foreground ,scuro))))
;;;;; which-func-mode
  `(which-func ((t (:foreground ,scuro :background ,chiaro))))
;;;;; yascroll
  `(yascroll:thumb-text-area ((t (:background ,chiaro))))
  `(yascroll:thumb-fringe ((t (:background ,chiaro :foreground ,scuro))))
  ))

;;; Theme Variables
(paper-dark/with-color-variables
  (custom-theme-set-variables
   'paper-dark
;;;;; ansi-color
   `(ansi-color-names-vector [,chiaro ,scuro ,scuro ,scuro ,scuro ,scuro ,scuro ,scuro])
;;;;; fill-column-indicator
   `(fci-rule-color ,scuro)
;;;;; vc-annotate
   `(vc-annotate-color-map
     '(( 30. . ,scuro)
       ( 60. . ,scuro)
       ( 90. . ,scuro)
       (120. . ,scuro)
       (150. . ,scuro)
       (180. . ,scuro)
       (210. . ,scuro)
       (240. . ,scuro)
       (270. . ,scuro)
       (300. . ,scuro)
       (330. . ,scuro)
       (360. . ,scuro)))
   `(vc-annotate-very-old-color ,scuro)
   `(vc-annotate-background ,chiaro)
   ))
;;; Footer
;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
		  (file-name-as-directory
		   (file-name-directory load-file-name))))
(provide-theme 'paper-dark)
;;; paper-dark-theme.el ends here
