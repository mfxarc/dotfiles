;;; paper-theme.el --- A color theme for Emacs based on papercolors.com -*- lexical-binding: t; no-byte-compile: t; -*-

(deftheme paper "Paper based theme.")
;;; Color Palette
;; #AD8B73
;; #CFC1B5
(defvar paper-colors-alist
  '(("scuro"           . "#322F37")
    ("rosso"           . "#9D4D38")
    ("verde"           . "#B9BAA2")
    ("olive"           . "#403E3B")
    ("marrone"         . "#8F7E66")
    ("dark-beige"      . "#CDC6C1")
    ("beige"           . "#D7CBC1")
    ("light"           . "#DAD3CD")
    ("chiaro"          . "#E7E0DA"))
  "List of Paper colors.
Each element has the form (NAME . HEX). ")
(defmacro paper/with-color-variables (&rest body)
  "`let' bind all colors defined in `paper-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
	 ,@(mapcar (lambda (cons)
		     (list (intern (car cons)) (cdr cons)))
		   paper-colors-alist))
     ,@body))
;; Setting special fonts
(set-fontset-font t 'hangul '("Sarasa Mono K" . "unicode-bmp"))
(set-fontset-font t 'symbol '("Symbola" . "unicode-bmp"))
;;; Theme Faces
(paper/with-color-variables
 (custom-theme-set-faces
  'paper
;;;; Built-in
  '(button ((t (:underline t))))
  ;; Defaults
  `(default ((t (:font "Iosevka" :height 170 :foreground ,scuro :background ,chiaro))))
  `(fixed-pitch ((t (:inherit default))))
  `(variable-pitch ((t (:font "Roboto" :height 170))))
  `(italic ((t (:inherit variable-pitch :slant italic))))
  `(solaire-default-face ((t (:background ,light))))
  ;; Windows
  `(fringe ((t (:inherit default :background nil))))
  `(window-divider ((t (:foreground ,dark-beige))))
  `(window-divider-first-pixel ((t (:inherit window-divider))))
  `(window-divider-last-pixel ((t (:inherit window-divider))))
  ;; Completion
  `(completions-annotations ((t (:inherit default :underline nil :slant italic :foreground ,scuro))))
  `(highlight ((t (:background ,dark-beige))))
  `(vertico-current ((t (:background ,dark-beige))))
  ;; Elfeed
  `(elfeed-search-feed-face ((t (:foreground ,scuro))))
  `(elfeed-search-tag-face ((t (:inherit font-lock-variable-name-face))))
  `(elfeed-search-date-face ((t (:inherit font-lock-comment-face))))
  `(elfeed-search-title-face ((t (:foreground ,marrone))))
  `(elfeed-search-unread-title-face ((t (:background nil :foreground ,scuro))))
  `(message-header-name ((t (:inherit org-document-info-keyword))))
  `(message-header-subject ((t (:inherit org-document-title))))
  `(message-header-to ((t (:inherit org-document-title))))
  `(message-header-other ((t (:inherit org-document-info))))
  ;; Org
  `(org-block ((t (:inherit default :background ,dark-beige))))
  `(org-verbatim ((t (:inherit default :foreground ,scuro :slant italic :box (:line-width 1 :color ,scuro)))))
  `(org-document-title ((t (:foreground ,scuro))))
  `(org-document-info ((t (:foreground ,scuro))))
  `(org-document-info-keyword ((t (:inherit org-document-info))))
  `(org-property-value ((t (:inherit org-document-info))))
  `(org-special-keyword ((t (:inherit org-document-info))))
  `(org-meta-line ((t (:inherit org-document-info-keyword))))
  `(org-drawer ((t (:inherit org-document-info-keyword))))
  `(org-indent ((t (:inherit (org-hide default)))))
  `(org-level-1 ((t (:foreground ,scuro :weight bold :height 1.0))))
  `(org-level-1 ((t (:height 1.0 :weight bold ))))
  `(org-level-2 ((t (:height 1.0 :weight bold))))
  `(org-level-3 ((t (:height 1.0 :weight bold))))
  `(org-level-4 ((t (:height 1.0 :weight bold))))
  `(org-level-5 ((t (:height 1.0 :weight bold))))
  `(org-level-6 ((t (:height 1.0 :weight bold))))
  `(org-level-7 ((t (:height 1.0 :weight bold))))
  `(org-level-8 ((t (:height 1.0 :weight bold))))
  `(org-agenda-date-today ((t (:foreground ,scuro :slant italic :weight bold))) t)
  `(org-agenda-structure ((t (:inherit font-lock-comment-face))))
  `(org-archived ((t (:foreground ,chiaro :weight bold))))
  `(org-checkbox ((t (:foreground ,scuro))))
  `(org-date ((t (:foreground ,marrone :underline t))))
  `(org-deadline-announce ((t (:foreground ,chiaro))))
  `(org-done ((t (:bold t :weight bold :foreground ,chiaro))))
  `(org-formula ((t (:foreground ,chiaro))))
  `(org-headline-done ((t (:foreground ,chiaro))))
  `(org-hide ((t (:foreground ,chiaro))))
  `(org-link ((t (:inherit link))))
  `(org-scheduled ((t (:foreground ,chiaro))))
  `(org-special-keyword ((t (:inherit font-lock-comment-face))))
  `(org-table ((t (:foreground ,scuro))))
  `(org-tag ((t (:bold t :weight bold))))
  `(org-todo ((t (:bold t :foreground ,chiaro :weight bold))))
  `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))
  `(org-warning ((t (:bold t :foreground ,chiaro :weight bold :underline nil))))
  `(org-footnote ((t (:foreground ,chiaro :weight bold))))
  ;; Magit
  `(magit-branch-remote ((t (:inherit font-lock-constant-face))))

  `(magit-diff-file-heading ((t (:inherit default))))
  `(magit-diff-file-heading-highlight ((t (:inherit hl-line))))
  `(magit-diff-hunk-heading ((t (:inherit hl-line))))
  `(magit-diff-hunk-heading-highlight ((t (:inherit hl-line))))
  `(magit-section-heading ((t (:inherit org-level-1))))
  `(magit-section-highlight ((t (:inherit hl-line))))

  `(magit-diff-context ((t (:inherit default :foreground ,olive))))
  `(magit-diff-context-highlight ((t (:inherit default :foreground ,olive))))
  `(magit-diff-removed-highlight ((t (:inherit diff-removed))))
  `(magit-diff-added-highlight ((t (:inherit diff-added))))
  `(magit-diff-removed ((t (:inherit diff-removed))))
  `(magit-diff-added ((t (:inherit diff-added))))

  `(diff-added ((t (:foreground ,scuro :background ,verde))))
  `(diff-removed ((t (:foreground ,scuro :background ,rosso))))
  `(diff-refine-removed ((t (:foreground ,scuro :background ,chiaro))))
  `(diff-changed ((t (:foreground ,chiaro))))
  `(diff-context ((t (:foreground ,scuro))))
  `(diff-refine-added ((t :inherit diff-added :background ,chiaro :weight bold)))
  `(diff-refine-change ((t :inherit diff-changed :weight bold)))
  `(diff-header ((,class (:foreground ,chiaro :weight bold))
		 (t (:foreground ,chiaro :weight bold))))
  `(diff-file-header
    ((,class (:foreground ,chiaro :weight bold))
     (t (:foreground ,chiaro :weight bold))))
  `(diff-hunk-header
    ((,class (:foreground ,chiaro :weight bold))
     (t (:foreground ,chiaro :weight bold))))
  `(diff-hl-insert ((t (:foreground ,chiaro :background ,chiaro))))
  `(diff-hl-delete ((t (:foreground ,chiaro :background ,chiaro))))
  `(diff-hl-change ((t (:foreground ,chiaro :background ,chiaro))))
  ;; EWW
  `(eww-valid-certificate ((t (:inherit default))))
  `(eww-form-text ((t (:inherit default :box (:line-width 1)))))
  `(eww-form-file ((t (:inherit eww-form-text))))
  `(eww-form-select ((t (:inherit eww-form-text))))
  `(eww-form-submit ((t (:inherit eww-form-text))))
  `(eww-form-checkbox ((t (:inherit eww-form-text))))
  `(eww-form-textarea ((t (:inherit eww-form-text))))
  ;; Tab-bar
  `(tab-bar ((t (:foreground ,chiaro :background ,chiaro))))
  `(tab-bar-tab ((t (:foreground ,scuro :background ,dark-beige))))
  ;; LaTeX
  `(font-latex-sectioning-0-face ((t (:inherit org-level-1))))
  `(font-latex-sectioning-1-face ((t (:inherit org-level-2))))
  `(font-latex-sectioning-2-face ((t (:inherit org-level-3))))
  `(font-latex-sectioning-3-face ((t (:inherit org-level-4))))
  `(font-latex-sectioning-4-face ((t (:inherit org-level-5))))
  `(font-latex-sectioning-5-face ((t (:inherit org-level-6))))
  `(font-latex-italic-face ((t (:inherit italic))))
  `(font-latex-bold-face ((t (:weight bold))))
  `(font-latex-warning-face ((t (:inherit warning))))
  ;; Hydra
  `(hydra-face-red ((t (:foreground ,chiaro))))
  `(hydra-face-blue ((t (:foreground ,chiaro))))
  `(hydra-face-pink ((t (:foreground ,chiaro))))
  `(shadow ((t (:foreground ,scuro))))
  `(link ((t (:foreground ,rosso :underline t))))
  `(link-visited ((t (:underline t))))
  `(cursor ((t (:foreground ,chiaro :background ,chiaro))))
  `(escape-glyph ((t (:foreground ,chiaro :bold t))))
  `(header-line ((t (:inherit mode-line))))
  `(success ((t (:foreground ,chiaro :weight bold))))
  `(warning ((t (:foreground ,chiaro :background ,marrone :weight bold))))
;;;;; compilation
  `(compilation-column-face ((t (:foreground ,chiaro))))
  `(compilation-enter-directory-face ((t (:foreground ,chiaro))))
  `(compilation-error-face ((t (:foreground ,chiaro :weight bold :underline t))))
  `(compilation-face ((t (:foreground ,chiaro))))
  `(compilation-info-face ((t (:foreground ,chiaro))))
  `(compilation-info ((t (:foreground ,chiaro :underline t))))
  `(compilation-leave-directory-face ((t (:foreground ,chiaro))))
  `(compilation-line-face ((t (:foreground ,chiaro))))
  `(compilation-line-number ((t (:foreground ,chiaro))))
  `(compilation-message-face ((t (:foreground ,chiaro))))
  `(compilation-warning-face ((t (:foreground ,chiaro :weight bold :underline t))))
  `(compilation-mode-line-exit ((t (:foreground ,chiaro :weight bold))))
  `(compilation-mode-line-fail ((t (:foreground ,chiaro :weight bold))))
  `(compilation-mode-line-run ((t (:foreground ,chiaro :weight bold))))
;;;;; grep
  `(grep-context-face ((t (:foreground ,chiaro))))
  `(grep-error-face ((t (:foreground ,chiaro :weight bold :underline t))))
  `(grep-hit-face ((t (:foreground ,chiaro :weight bold))))
  `(grep-match-face ((t (:foreground ,chiaro :weight bold))))
  `(match ((t (:background ,scuro :foreground ,chiaro))))
;;;;; isearch
  `(isearch ((t (:foreground ,chiaro :weight bold :background ,scuro))))
  `(isearch-fail ((t (:foreground ,chiaro :weight bold :background ,scuro))))
  `(lazy-highlight ((t (:foreground ,chiaro :weight bold :background ,chiaro))))

  `(menu ((t (:foreground ,chiaro :background ,scuro))))
  `(minibuffer-prompt ((t (:foreground ,scuro))))
  `(region ((,class (:background ,scuro :foreground ,chiaro))))
  `(secondary-selection ((t (:background ,chiaro))))
  `(trailing-whitespace ((t (:background ,chiaro))))
  `(vertical-border ((t (:foreground ,scuro))))
;;;;; font lock
  `(font-lock-builtin-face ((t (:foreground ,rosso))))
  `(font-lock-comment-face ((t (:foreground ,marrone :slant italic))))
  `(sh-heredoc ((t (:inherit font-lock-comment-face))))
  `(font-lock-comment-delimiter-face ((t (:inherit font-lock-comment-face))))
  `(font-lock-constant-face ((t (:foreground ,rosso))))
  `(font-lock-doc-face ((t (:inherit font-lock-comment-face))))
  `(font-lock-function-name-face ((t (:foreground ,rosso :weight normal))))
  `(font-lock-keyword-face ((t (:foreground "#797268"))))
  `(font-lock-string-face ((t (:foreground ,olive :background ,dark-beige))))
  `(font-lock-negation-char-face ((t (:foreground ,rosso :weight normal))))
  `(font-lock-preprocessor-face ((t (:foreground ,rosso :weight normal))))
  `(font-lock-regexp-grouping-construct ((t (:foreground ,rosso :weight normal))))
  `(font-lock-regexp-grouping-backslash ((t (:foreground ,rosso :weight normal))))
  `(font-lock-type-face ((t (:foreground ,rosso))))
  `(font-lock-variable-name-face ((t (:foreground ,rosso))))
  `(font-lock-warning-face ((t (:foreground ,rosso :weight normal))))
  `(c-annotation-face ((t (:inherit font-lock-constant-face))))
;;;;; ledger
  `(ledger-font-directive-face ((t (:foreground ,chiaro))))
  `(ledger-font-periodic-xact-face ((t (:inherit ledger-font-directive-face))))
  `(ledger-occur-xact-face ((t (:background ,scuro))))
;;;; Third-party
;;;;; ace-jump
  `(ace-jump-face-background
    ((t (:foreground ,scuro :background ,scuro :inverse-video nil))))
  `(ace-jump-face-foreground
    ((t (:foreground ,chiaro :background ,scuro :inverse-video nil))))
;;;;; anzu
  `(anzu-mode-line ((t (:foreground ,chiaro :weight bold))))
;;;;; auto-complete
  `(ac-candidate-face ((t (:background ,scuro :foreground ,chiaro))))
  `(ac-selection-face ((t (:background ,scuro :foreground ,chiaro))))
  `(popup-tip-face ((t (:background ,chiaro :foreground ,chiaro))))
  `(popup-scroll-bar-foreground-face ((t (:background ,scuro))))
  `(popup-scroll-bar-background-face ((t (:background ,scuro))))
  `(popup-isearch-match ((t (:background ,scuro :foreground ,chiaro))))
;;;;; clojure-test-mode
  `(clojure-test-failure-face ((t (:foreground ,chiaro :weight bold :underline t))))
  `(clojure-test-error-face ((t (:foreground ,chiaro :weight bold :underline t))))
  `(clojure-test-success-face ((t (:foreground ,chiaro :weight bold :underline t))))

;;;;; dired/dired+/dired-subtree
  `(diredp-display-msg ((t (:foreground ,chiaro))))
  `(diredp-compressed-file-suffix ((t (:foreground ,chiaro))))
  `(diredp-date-time ((t (:foreground ,chiaro))))
  `(diredp-deletion ((t (:foreground ,chiaro))))
  `(diredp-deletion-file-name ((t (:foreground ,chiaro))))
  `(diredp-dir-heading ((t (:foreground ,chiaro :background ,scuro :weight bold))))
  `(diredp-dir-priv ((t (:foreground ,chiaro))))
  `(diredp-exec-priv ((t (:foreground ,chiaro))))
  `(diredp-executable-tag ((t (:foreground ,chiaro))))
  `(diredp-file-name ((t (:foreground ,chiaro))))
  `(diredp-file-suffix ((t (:foreground ,chiaro))))
  `(diredp-flag-mark ((t (:foreground ,chiaro))))
  `(diredp-flag-mark-line ((t (:foreground ,chiaro))))
  `(diredp-ignored-file-name ((t (:foreground ,scuro))))
  `(diredp-link-priv ((t (:foreground ,chiaro))))
  `(diredp-mode-line-flagged ((t (:foreground ,chiaro))))
  `(diredp-mode-line-marked ((t (:foreground ,chiaro))))
  `(diredp-no-priv ((t (:foreground ,chiaro))))
  `(diredp-number ((t (:foreground ,chiaro))))
  `(diredp-other-priv ((t (:foreground ,chiaro))))
  `(diredp-rare-priv ((t (:foreground ,chiaro))))
  `(diredp-read-priv ((t (:foreground ,chiaro))))
  `(diredp-symlink ((t (:foreground ,scuro :background ,chiaro))))
  `(diredp-write-priv ((t (:foreground ,chiaro))))
  `(dired-subtree-depth-1-face ((t (:background ,scuro))))
  `(dired-subtree-depth-2-face ((t (:background ,scuro))))
  `(dired-subtree-depth-3-face ((t (:background ,scuro))))
;;;;; ediff
  `(ediff-current-diff-A ((t (:foreground ,chiaro :background ,chiaro))))
  `(ediff-current-diff-Ancestor ((t (:foreground ,chiaro :background ,chiaro))))
  `(ediff-current-diff-B ((t (:foreground ,chiaro :background ,chiaro))))
  `(ediff-current-diff-C ((t (:foreground ,chiaro :background ,chiaro))))
  `(ediff-even-diff-A ((t (:background ,scuro))))
  `(ediff-even-diff-Ancestor ((t (:background ,scuro))))
  `(ediff-even-diff-B ((t (:background ,scuro))))
  `(ediff-even-diff-C ((t (:background ,scuro))))
  `(ediff-fine-diff-A ((t (:foreground ,chiaro :background ,chiaro :weight bold))))
  `(ediff-fine-diff-Ancestor ((t (:foreground ,chiaro :background ,chiaro weight bold))))
  `(ediff-fine-diff-B ((t (:foreground ,chiaro :background ,chiaro :weight bold))))
  `(ediff-fine-diff-C ((t (:foreground ,chiaro :background ,chiaro :weight bold ))))
  `(ediff-odd-diff-A ((t (:background ,scuro))))
  `(ediff-odd-diff-Ancestor ((t (:background ,scuro))))
  `(ediff-odd-diff-B ((t (:background ,scuro))))
  `(ediff-odd-diff-C ((t (:background ,scuro))))
;;;;; eshell
  `(eshell-prompt ((t (:foreground ,chiaro :weight bold))))
  `(eshell-ls-archive ((t (:foreground ,chiaro :weight bold))))
  `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
  `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
  `(eshell-ls-directory ((t (:foreground ,chiaro :weight bold))))
  `(eshell-ls-executable ((t (:foreground ,chiaro))))
  `(eshell-ls-unreadable ((t (:foreground ,scuro))))
  `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
  `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
  `(eshell-ls-special ((t (:foreground ,chiaro :weight bold))))
  `(eshell-ls-symlink ((t (:foreground ,scuro :background ,chiaro))))
;;;;; evil
  `(evil-search-highlight-persist-highlight-face ((t (:inherit lazy-highlight))))
;;;;; flx
  `(flx-highlight-face ((t (:foreground ,chiaro :weight bold))))
;;;;; flycheck
  `(flycheck-error
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro) :inherit unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
  `(flycheck-warning
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro) :inherit unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
  `(flycheck-info
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro) :inherit unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
  `(flycheck-fringe-error ((t (:foreground ,chiaro :weight bold))))
  `(flycheck-fringe-warning ((t (:foreground ,chiaro :weight bold))))
  `(flycheck-fringe-info ((t (:foreground ,chiaro :weight bold))))
;;;;; flymake
  `(flymake-errline
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)
		  :inherit unspecified :foreground unspecified :background unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
  `(flymake-warnline
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)
		  :inherit unspecified :foreground unspecified :background unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
  `(flymake-infoline
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)
		  :inherit unspecified :foreground unspecified :background unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
;;;;; flyspell
  `(flyspell-duplicate
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro) :inherit unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
  `(flyspell-incorrect
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro) :inherit unspecified))
     (t (:foreground ,chiaro :weight bold :underline t))))
;;;;; git-gutter
  `(git-gutter:added ((t (:foreground ,chiaro :weight bold))))
  `(git-gutter:deleted ((t (:foreground ,chiaro :weight bold))))
  `(git-gutter:modified ((t (:foreground ,chiaro :weight bold))))
  `(git-gutter:unchanged ((t (:foreground ,chiaro :weight bold))))
;;;;; git-gutter-fr
  `(git-gutter-fr:added ((t (:foreground ,chiaro  :weight bold))))
  `(git-gutter-fr:deleted ((t (:foreground ,chiaro :weight bold))))
  `(git-gutter-fr:modified ((t (:foreground ,chiaro :weight bold))))
;;;;; guide-key
  `(guide-key/highlight-command-face ((t (:foreground ,chiaro))))
  `(guide-key/key-face ((t (:foreground ,chiaro))))
  `(guide-key/prefix-command-face ((t (:foreground ,chiaro))))
;;;;; highlight-symbol
  `(highlight-symbol-face ((t (:background "gray88" :underline t))))
;;;;; hl-line-mode
  `(hl-line ((t (:background ,dark-beige))))
;;;;; hl-sexp
  `(hl-sexp-face ((,class (:background ,scuro)) (t :weight bold)))
;;;;; ido-mode
  `(ido-first-match ((t (:foreground ,chiaro :weight bold))))
  `(ido-only-match ((t (:foreground ,chiaro :weight bold))))
  `(ido-subdir ((t (:foreground ,chiaro))))
  `(ido-indicator ((t (:foreground ,chiaro :background ,chiaro))))
;;;;; indent-guide
  `(indent-guide-face ((t (:foreground ,scuro))))
;;;;; js2-mode
  `(js2-warning ((t (:underline ,chiaro))))
  `(js2-error ((t (:foreground ,chiaro :weight bold))))
  `(js2-jsdoc-tag ((t (:foreground ,chiaro))))
  `(js2-jsdoc-type ((t (:foreground ,chiaro))))
  `(js2-jsdoc-value ((t (:foreground ,chiaro))))
  `(js2-function-param ((t (:foreground ,chiaro))))
  `(js2-external-variable ((t (:foreground ,chiaro))))
;;;;; linum-mode
  `(linum ((t (:foreground ,chiaro :background ,scuro))))
;;;;; magit
  `(magit-header ((t (:foreground ,chiaro :background nil :weight bold))))
  `(magit-section-title ((t (:foreground ,chiaro :background nil :weight bold))))
  `(magit-branch ((t (:foreground ,chiaro :background ,chiaro
				  :weight bold
				  :box (:line-width 1 :color ,chiaro)))))
  `(magit-item-highlight ((t (:background ,scuro))))
  `(magit-log-author ((t (:foreground ,chiaro))))
  `(magit-log-sha1 ((t (:foreground ,chiaro :weight bold))))
  `(magit-tag ((t (:foreground ,chiaro :weight bold))))
  `(magit-log-head-label-head ((t (:foreground ,chiaro :background ,chiaro
					       :weight bold
					       :box (:line-width 1 :color ,chiaro)))))
  `(magit-log-head-label-local ((t (:foreground ,chiaro :background ,chiaro
						:weight bold
						:box (:line-width 1 :color ,chiaro)))))
  `(magit-log-head-label-default ((t (:foreground ,chiaro :background ,chiaro
						  :weight bold
						  :box (:line-width 1 :color ,chiaro)))))
  `(magit-log-head-label-remote ((t (:foreground ,chiaro :background ,chiaro
						 :weight bold
						 :box (:line-width 1 :color ,chiaro)))))
  `(magit-log-head-label-tags ((t (:foreground ,chiaro :weight bold))))
;;;;; outline
  `(outline-1 ((t (:foreground ,chiaro))))
  `(outline-2 ((t (:foreground ,chiaro))))
  `(outline-3 ((t (:foreground ,chiaro))))
  `(outline-4 ((t (:foreground ,chiaro))))
  `(outline-5 ((t (:foreground ,chiaro))))
  `(outline-6 ((t (:foreground ,chiaro))))
;;;;; rainbow-delimiters
  `(rainbow-delimiters-depth-1-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-2-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-3-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-4-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-5-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-6-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-7-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-8-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-9-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-10-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-11-face ((t (:foreground ,chiaro))))
  `(rainbow-delimiters-depth-12-face ((t (:foreground ,chiaro))))
;;;;; structured-haskell-mode
  `(shm-current-face ((t (:background ,scuro))))
  `(shm-quarantine-face ((t (:inherit font-lock-error))))
;;;;; show-paren
  `(show-paren-mismatch ((t (:foreground ,chiaro :background ,chiaro :weight bold))))
  `(show-paren-match ((t (:foreground ,scuro :background ,chiaro :weight bold))))
;;;;; mode-line
  `(mode-line ((,class (:foreground ,scuro :background ,chiaro :font "Noto Sans" :height 92))))
  `(doom-modeline-bar ((t (:foreground nil :background nil))))
  `(doom-modeline-panel ((t (:foreground nil :background nil))))
  `(doom-modeline-bar-inactive ((t (:foreground nil :background nil))))
  `(mode-line-inactive ((t (:inherit mode-line :foreground ,scuro))))
  `(mode-line-buffer-id ((t (:foreground ,scuro))))
  `(header-line ((,class (:inherit mode-line))))
  `(persp-selected-face ((t (:foreground ,chiaro :weight light :height 92))))
;;;;; SLIME
  `(slime-repl-output-face ((t (:foreground ,chiaro))))
  `(slime-repl-inputed-output-face ((t (:foreground ,chiaro))))
  `(slime-error-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)))
     (t
      (:underline ,chiaro))))
  `(slime-warning-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)))
     (t
      (:underline ,chiaro))))
  `(slime-style-warning-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)))
     (t
      (:underline ,chiaro))))
  `(slime-note-face
    ((((supports :underline (:style wave)))
      (:underline (:style wave :color ,chiaro)))
     (t
      (:underline ,chiaro))))
  `(slime-highlight-face ((t (:inherit ,chiaro))))
;;;;; term
  `(term-color-black ((t (:foreground ,scuro))))
  `(term-color-red ((t (:foreground ,rosso))))
  `(term-color-green ((t (:foreground "sea green"))))
  `(term-color-yellow ((t (:foreground "goldenrod4"))))
  `(term-color-blue ((t (:foreground "powder blue"))))
  `(term-color-magenta ((t (:foreground "light slate blue"))))
  `(term-color-cyan ((t (:foreground "dark cyan"))))
  `(term-color-white ((t (:foreground ,chiaro))))
;;;;; web-mode
  `(web-mode-builtin-face ((t (:inherit ,font-lock-builtin-face))))
  `(web-mode-comment-face ((t (:inherit ,font-lock-comment-face))))
  `(web-mode-constant-face ((t (:inherit ,font-lock-constant-face))))
  `(web-mode-css-at-rule-face ((t (:foreground ,chiaro ))))
  `(web-mode-css-prop-face ((t (:foreground ,chiaro))))
  `(web-mode-css-pseudo-class-face ((t (:foreground ,chiaro :weight bold))))
  `(web-mode-css-rule-face ((t (:foreground ,chiaro))))
  `(web-mode-doctype-face ((t (:inherit ,font-lock-comment-face))))
  `(web-mode-folded-face ((t (:underline t))))
  `(web-mode-function-name-face ((t (:foreground ,chiaro :weight bold))))
  `(web-mode-html-attr-name-face ((t (:foreground ,chiaro))))
  `(web-mode-html-attr-value-face ((t (:inherit ,font-lock-string-face))))
  `(web-mode-html-tag-face ((t (:foreground ,chiaro :weight bold))))
  `(web-mode-keyword-face ((t (:inherit ,font-lock-keyword-face))))
  `(web-mode-preprocessor-face ((t (:inherit ,font-lock-preprocessor-face))))
  `(web-mode-string-face ((t (:inherit ,font-lock-string-face))))
  `(web-mode-type-face ((t (:inherit ,font-lock-type-face))))
  `(web-mode-variable-name-face ((t (:inherit ,font-lock-variable-name-face))))
  `(web-mode-server-background-face ((t (:background ,scuro))))
  `(web-mode-server-comment-face ((t (:inherit web-mode-comment-face))))
  `(web-mode-server-string-face ((t (:foreground ,chiaro))))
  `(web-mode-symbol-face ((t (:inherit font-lock-constant-face))))
  `(web-mode-warning-face ((t (:inherit font-lock-warning-face))))
  `(web-mode-whitespaces-face ((t (:background ,chiaro))))
  `(web-mode-block-face ((t (:background "gray88"))))
  `(web-mode-current-element-highlight-face ((t (:inverse-video t))))
;;;;; whitespace-mode
  `(whitespace-space ((t (:background ,scuro :foreground ,chiaro))))
  `(whitespace-hspace ((t (:background ,scuro :foreground ,chiaro))))
  `(whitespace-tab ((t (:background ,chiaro))))
  `(whitespace-newline ((t (:foreground ,chiaro))))
  `(whitespace-trailing ((t (:background ,chiaro))))
  `(whitespace-line ((t (:background nil :foreground ,chiaro))))
  `(whitespace-space-before-tab ((t (:background ,scuro :foreground ,chiaro))))
  `(whitespace-indentation ((t (:background ,scuro :foreground ,chiaro))))
  `(whitespace-empty ((t (:background ,chiaro))))
  `(whitespace-space-after-tab ((t (:background ,scuro :foreground ,chiaro))))
;;;;; which-func-mode
  `(which-func ((t (:foreground ,chiaro :background ,scuro))))
;;;;; yascroll
  `(yascroll:thumb-text-area ((t (:background ,scuro))))
  `(yascroll:thumb-fringe ((t (:background ,scuro :foreground ,chiaro))))
  ))

;;; Theme Variables
(paper/with-color-variables
  (custom-theme-set-variables
   'paper
;;;;; ansi-color
   `(ansi-color-names-vector [,scuro ,chiaro ,chiaro ,chiaro ,chiaro ,chiaro ,chiaro ,chiaro])
;;;;; fill-column-indicator
   `(fci-rule-color ,chiaro)
;;;;; vc-annotate
   `(vc-annotate-color-map
     '(( 30. . ,chiaro)
       ( 60. . ,chiaro)
       ( 90. . ,chiaro)
       (120. . ,chiaro)
       (150. . ,chiaro)
       (180. . ,chiaro)
       (210. . ,chiaro)
       (240. . ,chiaro)
       (270. . ,chiaro)
       (300. . ,chiaro)
       (330. . ,chiaro)
       (360. . ,chiaro)))
   `(vc-annotate-very-old-color ,chiaro)
   `(vc-annotate-background ,scuro)
   ))
;;; Footer
;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
		  (file-name-as-directory
		   (file-name-directory load-file-name))))
(provide-theme 'paper)
;;; paper-theme.el ends here
