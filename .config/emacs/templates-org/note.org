#+LATEX_CLASS_OPTIONS: [ 12pt, openright, a4paper, english, brazil ]
* Packages
  #+LATEX_HEADER:\usepackage[utf8]{inputenc}
  #+LATEX_HEADER:\usepackage{fontspec}
  #+LATEX_HEADER:\usepackage{xparse}
  #+LATEX_HEADER:\usepackage{listings}
  #+LATEX_HEADER:\usepackage{geometry}
  #+LATEX_HEADER:\usepackage{titlesec}
  #+LATEX_HEADER:\usepackage{titling}
  #+LATEX_HEADER:\usepackage{setspace}
  #+LATEX_HEADER:\usepackage{microtype}
  #+LATEX_HEADER:\usepackage{xcolor}
  #+LATEX_HEADER:\usepackage{quoting}
  #+LATEX_HEADER:\usepackage{mdframed}
  #+LATEX_HEADER:\usepackage{url}
  #+LATEX_HEADER:\usepackage [backend=biber, style=abnt, language=brazil, noslsn, repeatfields] {biblatex}
  #+LATEX_HEADER:\addbibresource{references/references.bib}

* Font and text style
  #+LATEX_HEADER:\setmainfont{Roboto}
  #+LATEX_HEADER:\setlength{\parindent}{1cm}
  #+LATEX_HEADER:\setlength{\parskip}{0.2cm}
  #+LATEX_HEADER:\urlstyle{sf}
* Page style
  #+LATEX_HEADER:\newgeometry{left=2cm,right=2cm,top=2cm,bottom=2cm}
  #+LATEX_HEADER:\renewcommand{\maketitle}{{{\raggedright\Huge\bfseries\thetitle{}}\vspace{1em}\\\theauthor\hspace{0.2cm}---\hspace{0.2cm}\thedate\par}}
* Define colors
  #+LATEX_HEADER:\definecolor{codepink}{HTML}{AF4DAE}
  #+LATEX_HEADER:\definecolor{codepurple}{HTML}{D1884D}
  #+LATEX_HEADER:\definecolor{codegray}{HTML}{6F7085}
  #+LATEX_HEADER:\definecolor{block}{HTML}{F5F5F5}
* Quotes and text blocks
** myframe
   #+LATEX_HEADER:\newenvironment{myframe}
   #+LATEX_HEADER:{\begin{center}
   #+LATEX_HEADER:\begin{minipage}{0.92\textwidth}
   #+LATEX_HEADER:\begin{mdframed}[backgroundcolor=block!100,linewidth=0pt]}
   #+LATEX_HEADER:{\end{mdframed}
   #+LATEX_HEADER:\end{minipage}
   #+LATEX_HEADER:\end{center}}
** myquote
   #+LATEX_HEADER:\renewenvironment{quote}
   #+LATEX_HEADER:{\begin{myframe}}
   #+LATEX_HEADER:{\end{myframe}}
* Code blocks
  #+LATEX_HEADER:\lstdefinestyle{mystyle}{
  #+LATEX_HEADER:backgroundcolor=\color{block},
  #+LATEX_HEADER:commentstyle=\em\color{codegray},
  #+LATEX_HEADER:keywordstyle=\color{codepink},
  #+LATEX_HEADER:stringstyle=\color{codepurple},
  #+LATEX_HEADER:breakatwhitespace=false,
  #+LATEX_HEADER:breaklines=true,
  #+LATEX_HEADER:captionpos=b,
  #+LATEX_HEADER:keepspaces=true,
  #+LATEX_HEADER:showspaces=false,
  #+LATEX_HEADER:showstringspaces=false,
  #+LATEX_HEADER:showtabs=false,
  #+LATEX_HEADER:tabsize=2
  #+LATEX_HEADER:}
  #+LATEX_HEADER:\lstset{style=mystyle}
* Lists
* 
